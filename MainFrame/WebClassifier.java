package MainFrame;

import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JFileChooser;
import java.io.File;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebClassifier extends javax.swing.JFrame {

    private static WebClassifier wc=null;
    private Document document;
    public ArrayList<Term> globalTermVectors;
    public ArrayList<Document> documentList;
    public ArrayList<Document> resultDocuments;
    public double singleSimilarityThreshold;
    public double categorySimilarityThreshold;
    private int lowFrequencyThreshold;
    private RealtimeClassifyThread realtimeThread;
    private ClassifyByFileThread byUrlThread;
    private ClassifyByDirectoryThread byDirectoryThread;
    private Boolean pause=true;
    public Boolean threadLock=false;
    private double previousFValue=0.0;
    public Boolean auto=false;

    private WebClassifier() {
        initComponents();
        initialParameters();
    }
    public static WebClassifier getInstance()
    {
        if(wc==null)
        {
            wc=new WebClassifier();
            return wc;
        }
        else return wc;
    }
    

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        urlText = new javax.swing.JTextField();
        mainFrame = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        bodyText = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        anchorText = new javax.swing.JTextArea();
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        paragraphText = new javax.swing.JTextArea();
        jPanel6 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        hrefText = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        getFeaturesBtn = new javax.swing.JButton();
        checkHTMLBtn = new javax.swing.JButton();
        jScrollPane14 = new javax.swing.JScrollPane();
        titleText = new javax.swing.JTextArea();
        jScrollPane15 = new javax.swing.JScrollPane();
        keywordText = new javax.swing.JTextArea();
        jScrollPane16 = new javax.swing.JScrollPane();
        descriptionText = new javax.swing.JTextArea();
        loadHtmlBtn = new javax.swing.JButton();
        loadFeaturesBtn = new javax.swing.JButton();
        getPageModelBtn = new javax.swing.JButton();
        updateUrlsBtn = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        loadVectorsBtn = new javax.swing.JButton();
        jScrollPane8 = new javax.swing.JScrollPane();
        termTable = new javax.swing.JTable();
        jScrollPane9 = new javax.swing.JScrollPane();
        recordTable = new javax.swing.JTable();
        jLabel13 = new javax.swing.JLabel();
        termCountText = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        termTitleText = new javax.swing.JTextField();
        termFilterCombo = new javax.swing.JComboBox();
        termFilterButton = new javax.swing.JButton();
        termResetBtn = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        globalTermTable = new javax.swing.JTable();
        jScrollPane11 = new javax.swing.JScrollPane();
        globalRecordTable = new javax.swing.JTable();
        jLabel12 = new javax.swing.JLabel();
        globalTermCountText = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        globalTermFilterCombo = new javax.swing.JComboBox();
        globalTermFilterButton1 = new javax.swing.JButton();
        saveGlobalVectorsBtn = new javax.swing.JButton();
        loadGlobalVectorsBtn = new javax.swing.JButton();
        globalTermResetBtn = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        globalTermDocumentCountText = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        kTitleText = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        kSpinner = new javax.swing.JSpinner();
        kFilterBtn = new javax.swing.JButton();
        kResetBtn = new javax.swing.JButton();
        jScrollPane12 = new javax.swing.JScrollPane();
        kSimilarityTable = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        thresholdText = new javax.swing.JTextField();
        jScrollPane13 = new javax.swing.JScrollPane();
        classTable = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        resultTable = new javax.swing.JTable();
        jLabel23 = new javax.swing.JLabel();
        resultNumberText = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        resultUrlText = new javax.swing.JTextField();
        jPanel13 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        autoCalculateBtn = new javax.swing.JButton();
        jLabel29 = new javax.swing.JLabel();
        deltaText = new javax.swing.JTextField();
        currentFValueText = new javax.swing.JTextField();
        jLabel28 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        targetFValueText = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        parametersCombo = new javax.swing.JComboBox();
        thresholdStep = new javax.swing.JTextField();
        jLabel32 = new javax.swing.JLabel();
        autoCalculateStopBtn = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        lowFrequencyThresholdText = new javax.swing.JTextField();
        categorySimilarityThresholdText = new javax.swing.JTextField();
        singleSimilarityThresholdText = new javax.swing.JTextField();
        saveConfigBtn = new javax.swing.JButton();
        loadConfigBtn = new javax.swing.JButton();
        jLabel30 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        aToPRatioThresholdText = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        aToInnerRatioThresholdText = new javax.swing.JTextField();
        jPanel14 = new javax.swing.JPanel();
        categoryCombo = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        fText = new javax.swing.JTextField();
        recallText = new javax.swing.JTextField();
        precisionText = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        datasourceText = new javax.swing.JTextField();
        chooseDataSourceBtn = new javax.swing.JButton();
        jLabel34 = new javax.swing.JLabel();
        hubFilterBtn = new javax.swing.JToggleButton();
        jLabel37 = new javax.swing.JLabel();
        termFilterBtn = new javax.swing.JToggleButton();
        closeBtn = new javax.swing.JButton();
        getHtmlsBtn = new javax.swing.JButton();
        initialTrainingVectors = new javax.swing.JButton();
        initialPageModelBtn = new javax.swing.JButton();
        showCategoryBtn = new javax.swing.JButton();
        loadTrainingVectorsBtn = new javax.swing.JButton();
        saveTrainingVectorsBtn = new javax.swing.JButton();
        classifyFileBtn = new javax.swing.JButton();
        classifyDirectoryBtn = new javax.swing.JButton();
        realTimeClassifyBtn = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        pauseBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Web Classifier"); // NOI18N
        setResizable(false);

        jLabel1.setText("URL"); // NOI18N

        urlText.setText("http://dl.pconline.com.cn/html_2/1/77/id=37742&pn=0.html"); // NOI18N

        mainFrame.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                mainFrameStateChanged(evt);
            }
        });

        jPanel1.setLayout(null);

        jLabel7.setText("Body"); // NOI18N

        bodyText.setColumns(20);
        bodyText.setLineWrap(true);
        bodyText.setRows(5);
        jScrollPane1.setViewportView(bodyText);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addContainerGap(977, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1001, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4);
        jPanel4.setBounds(10, 10, 1001, 200);

        jLabel3.setText("Anchors"); // NOI18N

        anchorText.setColumns(20);
        anchorText.setLineWrap(true);
        anchorText.setRows(5);
        jScrollPane2.setViewportView(anchorText);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3)
            .addComponent(jScrollPane2)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 216, 146, 354);

        jLabel5.setText("Paragraphs"); // NOI18N

        paragraphText.setColumns(20);
        paragraphText.setLineWrap(true);
        paragraphText.setRows(5);
        jScrollPane3.setViewportView(paragraphText);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addContainerGap(156, Short.MAX_VALUE))
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 216, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel3);
        jPanel3.setBounds(158, 216, 216, 354);

        jLabel6.setText("Hrefs"); // NOI18N

        hrefText.setColumns(20);
        hrefText.setLineWrap(true);
        hrefText.setRows(5);
        jScrollPane4.setViewportView(hrefText);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addContainerGap(161, Short.MAX_VALUE))
            .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 323, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(jPanel6);
        jPanel6.setBounds(380, 216, 201, 354);

        jLabel2.setText("Title"); // NOI18N

        jLabel4.setText("Description"); // NOI18N

        jLabel8.setText("Keywords"); // NOI18N

        getFeaturesBtn.setText("Get Features"); // NOI18N
        getFeaturesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getFeaturesBtnActionPerformed(evt);
            }
        });

        checkHTMLBtn.setText("Get HTML"); // NOI18N
        checkHTMLBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                checkHTMLBtnActionPerformed(evt);
            }
        });

        titleText.setColumns(20);
        titleText.setLineWrap(true);
        titleText.setRows(5);
        jScrollPane14.setViewportView(titleText);

        keywordText.setColumns(20);
        keywordText.setLineWrap(true);
        keywordText.setRows(5);
        jScrollPane15.setViewportView(keywordText);

        descriptionText.setColumns(20);
        descriptionText.setLineWrap(true);
        descriptionText.setRows(5);
        jScrollPane16.setViewportView(descriptionText);

        loadHtmlBtn.setText("Load HTML"); // NOI18N
        loadHtmlBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadHtmlBtnActionPerformed(evt);
            }
        });

        loadFeaturesBtn.setText("Load Features"); // NOI18N
        loadFeaturesBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadFeaturesBtnActionPerformed(evt);
            }
        });

        getPageModelBtn.setText("Get Page Model"); // NOI18N
        getPageModelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getPageModelBtnActionPerformed(evt);
            }
        });

        updateUrlsBtn.setText("Update URLs");
        updateUrlsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateUrlsBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane16, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                            .addComponent(jScrollPane14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                            .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(getFeaturesBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(checkHTMLBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(loadFeaturesBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(loadHtmlBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 134, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(updateUrlsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE)
                            .addComponent(getPageModelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(jLabel8)))
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8))
                    .addComponent(jLabel4))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(checkHTMLBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(loadHtmlBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 55, Short.MAX_VALUE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(getPageModelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 54, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(updateUrlsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                    .addComponent(loadFeaturesBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE)
                    .addComponent(getFeaturesBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 61, Short.MAX_VALUE))
                .addGap(32, 32, 32))
        );

        jPanel1.add(jPanel5);
        jPanel5.setBounds(587, 216, 424, 354);

        mainFrame.addTab("Contents", jPanel1);

        jPanel9.setLayout(null);

        loadVectorsBtn.setText("Load"); // NOI18N
        loadVectorsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadVectorsBtnActionPerformed(evt);
            }
        });
        jPanel9.add(loadVectorsBtn);
        loadVectorsBtn.setBounds(440, 540, 110, 23);

        termTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Term", "Frequency", "Inverse Document Frequency", "Information Gain", "CHI-Information", "CHI-Download", "CHI-Yellow"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        termTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                termTableMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(termTable);

        jPanel9.add(jScrollPane8);
        jScrollPane8.setBounds(10, 50, 1000, 340);

        recordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Record", "Frequency", "TF-IDF"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane9.setViewportView(recordTable);

        jPanel9.add(jScrollPane9);
        jScrollPane9.setBounds(10, 400, 1000, 130);

        jLabel13.setText("Term Count:"); // NOI18N
        jPanel9.add(jLabel13);
        jLabel13.setBounds(320, 20, 80, 15);

        termCountText.setEditable(false);
        jPanel9.add(termCountText);
        termCountText.setBounds(400, 20, 90, 21);

        jLabel14.setText("Filter:"); // NOI18N
        jPanel9.add(jLabel14);
        jLabel14.setBounds(510, 20, 48, 15);

        jLabel15.setText("Title:"); // NOI18N
        jPanel9.add(jLabel15);
        jLabel15.setBounds(20, 20, 36, 15);

        termTitleText.setEditable(false);
        jPanel9.add(termTitleText);
        termTitleText.setBounds(70, 20, 230, 21);

        termFilterCombo.setEditable(true);
        termFilterCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Frequency= :", "IDF= :", "IG= :" }));
        jPanel9.add(termFilterCombo);
        termFilterCombo.setBounds(560, 20, 190, 21);

        termFilterButton.setText("Filter"); // NOI18N
        termFilterButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                termFilterButtonActionPerformed(evt);
            }
        });
        jPanel9.add(termFilterButton);
        termFilterButton.setBounds(760, 20, 100, 23);

        termResetBtn.setText("Reset"); // NOI18N
        termResetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                termResetBtnActionPerformed(evt);
            }
        });
        jPanel9.add(termResetBtn);
        termResetBtn.setBounds(870, 20, 110, 23);

        mainFrame.addTab("Feature Vectors", jPanel9);

        jPanel11.setLayout(null);

        globalTermTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Term", "Frequency", "Inverse Document Frequency", "Information Gain", "CHI-Information", "CHI-Download", "CHI-Yellow"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        globalTermTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                globalTermTableMouseClicked(evt);
            }
        });
        jScrollPane10.setViewportView(globalTermTable);

        jPanel11.add(jScrollPane10);
        jScrollPane10.setBounds(10, 60, 1000, 330);

        globalRecordTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Record", "Frequency", "TF-IDF"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane11.setViewportView(globalRecordTable);

        jPanel11.add(jScrollPane11);
        jScrollPane11.setBounds(10, 400, 1000, 130);

        jLabel12.setText("Term Count:"); // NOI18N
        jPanel11.add(jLabel12);
        jLabel12.setBounds(20, 20, 80, 15);

        globalTermCountText.setEditable(false);
        jPanel11.add(globalTermCountText);
        globalTermCountText.setBounds(100, 20, 100, 21);

        jLabel16.setText("Filter:"); // NOI18N
        jPanel11.add(jLabel16);
        jLabel16.setBounds(510, 20, 48, 15);

        globalTermFilterCombo.setEditable(true);
        globalTermFilterCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Frequency= :", "IDF= :", "IG= :" }));
        jPanel11.add(globalTermFilterCombo);
        globalTermFilterCombo.setBounds(560, 20, 190, 21);

        globalTermFilterButton1.setText("Filter"); // NOI18N
        globalTermFilterButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                globalTermFilterButton1ActionPerformed(evt);
            }
        });
        jPanel11.add(globalTermFilterButton1);
        globalTermFilterButton1.setBounds(760, 20, 100, 23);

        saveGlobalVectorsBtn.setText("Save"); // NOI18N
        saveGlobalVectorsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveGlobalVectorsBtnActionPerformed(evt);
            }
        });
        jPanel11.add(saveGlobalVectorsBtn);
        saveGlobalVectorsBtn.setBounds(370, 540, 110, 23);

        loadGlobalVectorsBtn.setText("Load"); // NOI18N
        loadGlobalVectorsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadGlobalVectorsBtnActionPerformed(evt);
            }
        });
        jPanel11.add(loadGlobalVectorsBtn);
        loadGlobalVectorsBtn.setBounds(500, 540, 110, 23);

        globalTermResetBtn.setText("Reset"); // NOI18N
        globalTermResetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                globalTermResetBtnActionPerformed(evt);
            }
        });
        jPanel11.add(globalTermResetBtn);
        globalTermResetBtn.setBounds(870, 20, 100, 23);

        jLabel9.setText("IDF Sum:"); // NOI18N
        jPanel11.add(jLabel9);
        jLabel9.setBounds(310, 20, 60, 15);

        globalTermDocumentCountText.setEditable(false);
        jPanel11.add(globalTermDocumentCountText);
        globalTermDocumentCountText.setBounds(380, 20, 100, 21);

        mainFrame.addTab("Template", jPanel11);

        jPanel12.setLayout(null);

        jLabel17.setText("Title:"); // NOI18N
        jPanel12.add(jLabel17);
        jLabel17.setBounds(20, 20, 36, 15);

        kTitleText.setEditable(false);
        jPanel12.add(kTitleText);
        kTitleText.setBounds(70, 20, 360, 21);

        jLabel18.setText("K :"); // NOI18N
        jPanel12.add(jLabel18);
        jLabel18.setBounds(450, 20, 20, 15);
        jPanel12.add(kSpinner);
        kSpinner.setBounds(480, 20, 100, 22);

        kFilterBtn.setText("Filter"); // NOI18N
        kFilterBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kFilterBtnActionPerformed(evt);
            }
        });
        jPanel12.add(kFilterBtn);
        kFilterBtn.setBounds(790, 20, 100, 23);

        kResetBtn.setText("Reset"); // NOI18N
        kResetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kResetBtnActionPerformed(evt);
            }
        });
        jPanel12.add(kResetBtn);
        kResetBtn.setBounds(900, 20, 100, 23);

        kSimilarityTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Category", "Title", "Similarity"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane12.setViewportView(kSimilarityTable);

        jPanel12.add(jScrollPane12);
        jScrollPane12.setBounds(10, 60, 1000, 390);

        jLabel19.setText("Threshold :"); // NOI18N
        jPanel12.add(jLabel19);
        jLabel19.setBounds(590, 20, 70, 15);
        jPanel12.add(thresholdText);
        thresholdText.setBounds(670, 20, 100, 21);

        classTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Category", "Total Similarity", "Category"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane13.setViewportView(classTable);

        jPanel12.add(jScrollPane13);
        jScrollPane13.setBounds(10, 460, 1000, 100);

        mainFrame.addTab("Classification", jPanel12);

        jPanel7.setLayout(null);

        resultTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Title", "True Category", "Classified Category", "Similarity", "Score", "S-Info", "S-Down", "S-Yellow"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        resultTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                resultTableMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(resultTable);

        jPanel7.add(jScrollPane5);
        jScrollPane5.setBounds(10, 45, 1000, 520);

        jLabel23.setText("Total Number:");
        jPanel7.add(jLabel23);
        jLabel23.setBounds(20, 10, 90, 15);

        resultNumberText.setEditable(false);
        jPanel7.add(resultNumberText);
        resultNumberText.setBounds(120, 10, 80, 21);

        jLabel24.setText("URL:");
        jPanel7.add(jLabel24);
        jLabel24.setBounds(230, 10, 40, 15);

        resultUrlText.setEditable(false);
        jPanel7.add(resultUrlText);
        resultUrlText.setBounds(280, 10, 730, 21);

        mainFrame.addTab("Results", jPanel7);

        jPanel13.setLayout(null);

        autoCalculateBtn.setText("Auto Calculate Parameters");
        autoCalculateBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoCalculateBtnActionPerformed(evt);
            }
        });

        jLabel29.setText("Increase to Previous:");

        jLabel28.setText("Current F1 Value:");

        jLabel27.setText("Target F1 Value:");

        jLabel26.setText("Step:");

        parametersCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Single Page Similarity Threshold", "Category Similarity Threshold", "Term Lowest Frequency Threshold" }));

        jLabel32.setFont(new java.awt.Font("宋体", 0, 24));
        jLabel32.setText("Auto Parameters Adjustment");

        autoCalculateStopBtn.setText("Stop");
        autoCalculateStopBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoCalculateStopBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel32, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addComponent(parametersCombo, javax.swing.GroupLayout.Alignment.TRAILING, 0, 400, Short.MAX_VALUE)
                    .addGroup(jPanel8Layout.createSequentialGroup()
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel27, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(currentFValueText, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                            .addComponent(targetFValueText, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)
                            .addComponent(thresholdStep, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                        .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(deltaText, javax.swing.GroupLayout.DEFAULT_SIZE, 262, Short.MAX_VALUE))
                    .addComponent(autoCalculateBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE)
                    .addComponent(autoCalculateStopBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 400, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel32)
                .addGap(18, 18, 18)
                .addComponent(parametersCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel26)
                    .addComponent(thresholdStep, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27)
                    .addComponent(targetFValueText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(currentFValueText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(deltaText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(autoCalculateBtn)
                .addGap(18, 18, 18)
                .addComponent(autoCalculateStopBtn)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        jPanel13.add(jPanel8);
        jPanel8.setBounds(580, 20, 420, 410);

        jLabel21.setText("Single Page Similarity Threshold:"); // NOI18N

        jLabel22.setText("Category Similarity Threshold:");

        jLabel25.setText("Term Lowest Frequency Threshold:");

        saveConfigBtn.setText("Save");
        saveConfigBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveConfigBtnActionPerformed(evt);
            }
        });

        loadConfigBtn.setText("Load");
        loadConfigBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadConfigBtnActionPerformed(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("宋体", 0, 24));
        jLabel30.setText("Parameters");

        jLabel35.setText("Anchors/Paragraphs Ratio:");

        jLabel36.setText("Anchors/Whole Ratio:");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel30, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(saveConfigBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(47, 47, 47)
                        .addComponent(loadConfigBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(singleSimilarityThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(categorySimilarityThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel36, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                            .addComponent(jLabel35, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                            .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lowFrequencyThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(aToPRatioThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(aToInnerRatioThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel30)
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21)
                    .addComponent(singleSimilarityThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel22)
                    .addComponent(categorySimilarityThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel25)
                    .addComponent(lowFrequencyThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel35)
                    .addComponent(aToPRatioThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel36)
                    .addComponent(aToInnerRatioThresholdText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(saveConfigBtn)
                    .addComponent(loadConfigBtn))
                .addContainerGap())
        );

        jPanel13.add(jPanel10);
        jPanel10.setBounds(20, 20, 400, 290);

        categoryCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "information", "download", "yellow", "others" }));
        categoryCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                categoryComboActionPerformed(evt);
            }
        });

        jLabel10.setText("Precision:"); // NOI18N

        jLabel11.setText("Recall:"); // NOI18N

        jLabel20.setText("F1 Value:"); // NOI18N

        jLabel31.setFont(new java.awt.Font("宋体", 0, 24));
        jLabel31.setText("Evaluation");

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(categoryCombo, javax.swing.GroupLayout.Alignment.TRAILING, 0, 380, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                        .addComponent(precisionText, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                        .addComponent(recallText, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 120, Short.MAX_VALUE)
                        .addComponent(fText, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(categoryCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(6, 6, 6))
                    .addComponent(precisionText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addGap(6, 6, 6))
                    .addComponent(recallText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel20)
                        .addGap(6, 6, 6))
                    .addComponent(fText, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        jPanel13.add(jPanel14);
        jPanel14.setBounds(20, 330, 400, 230);

        jLabel33.setText("Data Source:");
        jPanel13.add(jLabel33);
        jLabel33.setBounds(590, 470, 80, 15);

        datasourceText.setEditable(false);
        datasourceText.setText("pages");
        jPanel13.add(datasourceText);
        datasourceText.setBounds(680, 470, 220, 21);

        chooseDataSourceBtn.setText("Choose");
        chooseDataSourceBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseDataSourceBtnActionPerformed(evt);
            }
        });
        jPanel13.add(chooseDataSourceBtn);
        chooseDataSourceBtn.setBounds(911, 470, 80, 23);

        jLabel34.setText("Term Prefilter:");
        jPanel13.add(jLabel34);
        jLabel34.setBounds(800, 520, 100, 15);

        hubFilterBtn.setText("On");
        jPanel13.add(hubFilterBtn);
        hubFilterBtn.setBounds(680, 520, 80, 23);

        jLabel37.setText("Hub Filter:");
        jPanel13.add(jLabel37);
        jLabel37.setBounds(590, 520, 70, 15);

        termFilterBtn.setText("On");
        termFilterBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                termFilterBtnActionPerformed(evt);
            }
        });
        jPanel13.add(termFilterBtn);
        termFilterBtn.setBounds(910, 520, 80, 23);

        mainFrame.addTab("Parameters And Evaluation", jPanel13);

        closeBtn.setText("Close"); // NOI18N
        closeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeBtnActionPerformed(evt);
            }
        });

        getHtmlsBtn.setText("Initial Training Pages"); // NOI18N
        getHtmlsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                getHtmlsBtnActionPerformed(evt);
            }
        });

        initialTrainingVectors.setText("Initial Training Vectors"); // NOI18N
        initialTrainingVectors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initialTrainingVectorsActionPerformed(evt);
            }
        });

        initialPageModelBtn.setText("Initial Page Model"); // NOI18N
        initialPageModelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                initialPageModelBtnActionPerformed(evt);
            }
        });

        showCategoryBtn.setText("Show Category"); // NOI18N
        showCategoryBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showCategoryBtnActionPerformed(evt);
            }
        });

        loadTrainingVectorsBtn.setText("Load Training Vectors"); // NOI18N
        loadTrainingVectorsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loadTrainingVectorsBtnActionPerformed(evt);
            }
        });

        saveTrainingVectorsBtn.setText("Save Training Vectors"); // NOI18N
        saveTrainingVectorsBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveTrainingVectorsBtnActionPerformed(evt);
            }
        });

        classifyFileBtn.setText("Classify By URLs"); // NOI18N
        classifyFileBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classifyFileBtnActionPerformed(evt);
            }
        });

        classifyDirectoryBtn.setText("Classify By Directory"); // NOI18N
        classifyDirectoryBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                classifyDirectoryBtnActionPerformed(evt);
            }
        });

        realTimeClassifyBtn.setText("Realtime Classify By Directory");
        realTimeClassifyBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                realTimeClassifyBtnActionPerformed(evt);
            }
        });

        progressBar.setStringPainted(true);

        pauseBtn.setText("Pause");
        pauseBtn.setEnabled(false);
        pauseBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(urlText, javax.swing.GroupLayout.PREFERRED_SIZE, 983, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(getHtmlsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(initialTrainingVectors, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(loadTrainingVectorsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(saveTrainingVectorsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(showCategoryBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(initialPageModelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(classifyFileBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(classifyDirectoryBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(progressBar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(realTimeClassifyBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pauseBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)
                                    .addComponent(closeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 142, Short.MAX_VALUE)))
                            .addComponent(mainFrame, javax.swing.GroupLayout.PREFERRED_SIZE, 1021, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(urlText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(mainFrame, javax.swing.GroupLayout.PREFERRED_SIZE, 600, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pauseBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addComponent(realTimeClassifyBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(saveTrainingVectorsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                        .addComponent(initialPageModelBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                        .addComponent(classifyFileBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                    .addComponent(getHtmlsBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(closeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(progressBar, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addComponent(classifyDirectoryBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(initialTrainingVectors, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(loadTrainingVectorsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                        .addComponent(showCategoryBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void initialParameters()
    {
        try
        {
            Config config=new Config("Parameters.props");
            singleSimilarityThreshold=Double.parseDouble(config.getValue("singleSimilarityThreshold"));
            categorySimilarityThreshold=Double.parseDouble(config.getValue("categorySimilarityThreshold"));
            lowFrequencyThreshold=Integer.parseInt(config.getValue("lowFrequencyThreshold"));
            String aToPRatioThreshold=config.getValue("aToPRatioThreshold");
            String aToInnerRatioThreshold=config.getValue("aToInnerRatioThreshold");
            singleSimilarityThresholdText.setText(String.valueOf(singleSimilarityThreshold));
            thresholdText.setText(String.valueOf(singleSimilarityThreshold));
            categorySimilarityThresholdText.setText(String.valueOf(categorySimilarityThreshold));
            lowFrequencyThresholdText.setText(String.valueOf(lowFrequencyThreshold));
            aToPRatioThresholdText.setText(aToPRatioThreshold);
            aToInnerRatioThresholdText.setText(aToInnerRatioThreshold);
        }
        catch(Exception ex){}
    }
    private void emptyText()
    {
        anchorText.setText("");
        paragraphText.setText("");
        hrefText.setText("");
        bodyText.setText("");
        keywordText.setText("");
        descriptionText.setText("");
        titleText.setText("");
    }
    private void updateKSimilarityTable()
    {
        kTitleText.setText(document.getTitle());
        DefaultTableModel tm=(DefaultTableModel)kSimilarityTable.getModel();
        tm.setRowCount(0);
        for(Document d:documentList)
        {
            Object[] arr=new Object[4];
            arr[0]=d.getTrueCategory();
            arr[1]=d.getTitle();
            arr[2]=d.getSimilarity();
            tm.addRow(arr);
        }
        kSpinner.setValue(tm.getRowCount());
        kSimilarityTable.invalidate();
    }
    private void updateGlobalTermTable()
    {
        globalTermCountText.setText(String.valueOf(globalTermVectors.size()));
        int termDocCount=0;
        DefaultTableModel tm=(DefaultTableModel)globalTermTable.getModel();
        tm.setRowCount(0);
        for(Term t:globalTermVectors)
        {
            Object[] arr=new Object[7];
            arr[0]=t.getName();
            int frequency=0;
            for(Record r:t.getRecordsList())
            {
                frequency+=r.getFrequency();
            }
            arr[1]=frequency;
            arr[2]=t.getRecordsList().size();
            termDocCount+=t.getRecordsList().size();
            arr[3]=t.getIG();
            arr[4]=t.getCHIInfo();
            arr[5]=t.getCHIDown();
            arr[6]=t.getCHIYellow();
            tm.addRow(arr);
        }
        globalTermDocumentCountText.setText(String.valueOf(termDocCount));
        globalTermTable.invalidate();
    }
    private void updateGlobalRecordTable(String term)
    {
        DefaultTableModel tm=(DefaultTableModel)globalRecordTable.getModel();
        tm.setRowCount(0);
        for(Term t:globalTermVectors)
        {
            if(t.getName().equals(term))
            {
                for(Record r:t.getRecordsList())
                {
                    Object[] arr=new Object[3];
                    arr[0]=r.getDocNum();
                    arr[1]=r.getFrequency();
                    arr[2]=r.getTFIDF();
                    tm.addRow(arr);
                } 
                break;
            }
        }
        globalRecordTable.invalidate();
    }

    private void closeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeBtnActionPerformed
        if(JOptionPane.showConfirmDialog(this, "Confirm to Close?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
        {
            System.exit(0);
        }
    }//GEN-LAST:event_closeBtnActionPerformed

    private JFileChooser setCurrentDialog(String dialog)
    {
        JFileChooser jf=new JFileChooser();
        File f=new File("./"+dialog+"/");
        jf.setCurrentDirectory(f);
        return jf;
    }
    private void updateTermTable(Document d)
    {
        termTitleText.setText(d.getTitle());
       
        ArrayList<Term> termvector=d.getTermVector();
        termCountText.setText(String.valueOf(termvector.size()));
        DefaultTableModel tableModel=(DefaultTableModel)termTable.getModel();
        tableModel.setRowCount(0);
        for(Term t:termvector)
        {
            Object[] arr=new Object[7];
            arr[0]=t.getName();//term
            arr[1]=t.getRecordsList().get(0).getFrequency();//frequency
            if(globalTermVectors!=null)
            {
                for(Term gt:globalTermVectors)
                {
                    if(gt.getName().equals(t.getName()))
                    {
                        arr[2]=gt.getRecordsList().size();//idf
                        arr[3]=gt.getIG();//information gain
                        arr[4]=gt.getCHIInfo();//chi Info
                        arr[5]=gt.getCHIDown();//chi down
                        arr[6]=gt.getCHIYellow();//chi yellow
                        break;
                    }
                    else
                    {
                        arr[2]=0;
                        arr[3]=0.0;
                        arr[4]=0.0;
                        arr[5]=0.0;
                        arr[6]=0.0;
                    }
                }
            }
            else
            {
                arr[2]=0;
                arr[3]=0.0;
            }
            tableModel.addRow(arr);
        }
        termTable.invalidate();
       
    }
    private void getHtmlsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getHtmlsBtnActionPerformed
        JFileChooser jf=setCurrentDialog("urls");
        String fileName=null;
        String category=null;
        if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION)
        {
            fileName=jf.getSelectedFile().getPath();
            category=jf.getSelectedFile().getName().split(".txt")[0];
        }
        if(fileName!=null)
        {
            new WriteOut().writeHtmls(fileName,category);
            JOptionPane.showMessageDialog(this, "Initial Training Pages Completed!");
        }
    }//GEN-LAST:event_getHtmlsBtnActionPerformed

    private void initialTrainingVectorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initialTrainingVectorsActionPerformed
        try
        {
            if(JOptionPane.showConfirmDialog(this, "Initial Training Vectors may be time consuming, do you Continue?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
            {
                //process pages into data models : globalTermVectors, documentList
                GetTermVectors gtv=new GetTermVectors();
                Long t1=System.currentTimeMillis();
                //globalTermVectors=gtv.getGlobalTermVectors();
                //globalTermVectors=gtv.getGlobalTermVectorsWithIG(); //20s
                globalTermVectors=gtv.getGlobalTermVectorsWithCHI();
                Long t2=System.currentTimeMillis();
                System.out.print("Get GlobalTermVectorsWithCHI: ");
                System.out.println(t2-t1);
                documentList=gtv.getDocumentList();  //0s
                JOptionPane.showMessageDialog(this, "Initial Training Vectors Completed!");
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }//GEN-LAST:event_initialTrainingVectorsActionPerformed

    
    private Boolean isMax(double test, double b, double c)
    {
        if(test>b&&test>c)
            return true;
        else return false;
    }
    private void showCategoryBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showCategoryBtnActionPerformed
        System.out.print("Initial Scores: ");
        Long t1=System.currentTimeMillis();
        if(documentList!=null &&document!=null)
        {          
            documentList=new GetSimilarity().getSimilarity(document, documentList);
            updateKSimilarityTable();
            System.out.println(System.currentTimeMillis()-t1);
            System.out.println("Initial Scores Completed!");
            System.out.println("Obtaining Category...");
            String category=kFilter();
            mainFrame.setSelectedIndex(3);
            JOptionPane.showMessageDialog(this,"This is a(an) "+category+" page!");
        }
        else JOptionPane.showMessageDialog(this, "Please initial or load Page Model or Training Vectors first!");
        
    }//GEN-LAST:event_showCategoryBtnActionPerformed

    private void initialPageModelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_initialPageModelBtnActionPerformed
        try
        {
            Long t1=System.currentTimeMillis();
            document=new GetHtml().getDocumentByUrl(urlText.getText(), globalTermVectors);
            System.out.print("Initial Page Model: ");
            System.out.println(System.currentTimeMillis()-t1);
            JOptionPane.showMessageDialog(this,"Initial Page Model Completed!");
        }
        catch(Exception e)
        {

        }

    }//GEN-LAST:event_initialPageModelBtnActionPerformed

    private void loadTrainingVectorsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadTrainingVectorsBtnActionPerformed
        if(JOptionPane.showConfirmDialog(this, "Load Training Vectors may be time consuming, do you Continue?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
        {
            Long t1=System.currentTimeMillis();
            ReadIn ri=new ReadIn();
            documentList=ri.loadDocumentListByDatabase(); //4s
            Long t2=System.currentTimeMillis();
            System.out.print("Load Document List: ");
            System.out.println(t2-t1);
            System.out.print("Load GlobalTermVectors: ");
            Long t11=System.currentTimeMillis();
            //globalTermVectors=ri.loadSimpleGlobalTermVectorsByDocumentList(documentList); //100s
            globalTermVectors=ri.loadSimpleGlobalTermVectorsByDocumentList(documentList);
            Long t22=System.currentTimeMillis();
            System.out.println(t22-t11);
            if(globalTermVectors!=null&&globalTermTable.getRowCount()==0) {
                updateGlobalTermTable();
            }
            JOptionPane.showMessageDialog(this,"Load Training Vectors Completed!");
        }
    }//GEN-LAST:event_loadTrainingVectorsBtnActionPerformed

    private void saveTrainingVectorsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveTrainingVectorsBtnActionPerformed
        if(globalTermVectors!=null&&documentList!=null)
        {
            if(JOptionPane.showConfirmDialog(this, "Save Training Vectors may be time consuming, do you Continue?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION)
            {
                //save data models into sql server 2005
                Long t2=System.currentTimeMillis();
                WriteOut wo=new WriteOut();
                Long t4=System.currentTimeMillis();
                System.out.print("Initial Database: ");
                System.out.println(t4-t2);
                wo.saveGlobalTermVectors(globalTermVectors); //40s
                Long t5=System.currentTimeMillis();
                System.out.print("Save GlobalTermVectors: ");
                System.out.println(t5-t4);
                wo.saveDocumentList(documentList); //0.14s
                Long t6=System.currentTimeMillis();
                System.out.print("Save Document List: ");
                System.out.println(t6-t5);
                System.out.println("Save Global Term Vectors Completed!");
                System.out.println("Save Documents List Completed!");
                JOptionPane.showMessageDialog(this, "Save Training Vectors Completed!");
            }
        }
        else JOptionPane.showMessageDialog(this, "Template Null!");
    }//GEN-LAST:event_saveTrainingVectorsBtnActionPerformed

    private String kFilter()
    {
        String category=null;
        DefaultTableModel tm=(DefaultTableModel)kSimilarityTable.getModel();
        Vector vector=tm.getDataVector();
        for(int i=0;i<vector.size();i++) {
            Vector v=(Vector)vector.get(i);
            if(Double.parseDouble(v.get(2).toString())<Double.parseDouble(thresholdText.getText())) {
                tm.removeRow(i);
                i--;
            }
        }
        kSpinner.setValue(tm.getRowCount());
        //calculate classTable
        double informationSum=0.0;
        double downloadSum=0.0;
        double yellowSum=0.0;
        for(Object o:vector) {
            Vector v=(Vector)o;
            if(v.get(0).equals("information"))
                informationSum+=Double.parseDouble(v.get(2).toString());
            else if(v.get(0).equals("download"))
                downloadSum+=Double.parseDouble(v.get(2).toString());
            else if(v.get(0).equals("yellow"))
                yellowSum+=Double.parseDouble(v.get(2).toString());
        }
        DefaultTableModel dtm=(DefaultTableModel)classTable.getModel();
        dtm.setRowCount(0);
        if(informationSum<categorySimilarityThreshold)
            informationSum=0;
        if(downloadSum<categorySimilarityThreshold)
            downloadSum=0;
        if(yellowSum<categorySimilarityThreshold)
            yellowSum=0;
        for(int i=0;i<4;i++)
        {
            Object[] arr=new Object[3];
            if(i==0)
            {
                arr[0]="information";
                arr[1]=informationSum;
                arr[2]=isMax(informationSum,downloadSum,yellowSum);
                if(arr[2].equals(true))
                    category="information";
            } 
            else if(i==1)
            {
                arr[0]="download";
                arr[1]=downloadSum;
                arr[2]=isMax(downloadSum,informationSum,yellowSum);
                if(arr[2].equals(true))
                    category="download";
            } 
            else if(i==2)
            {
                arr[0]="yellow";
                arr[1]=yellowSum;
                arr[2]=isMax(yellowSum,informationSum,downloadSum);
                if(arr[2].equals(true))
                    category="yellow";
            } 
            else
            {
                arr[0]="others";
                arr[1]=0;
                if(informationSum==yellowSum&&yellowSum==downloadSum)
                {
                    arr[2]=true;
                    category="others";
                }
                else arr[2]=false;
            }
            dtm.addRow(arr);
        }
        classTable.invalidate();
        if(hubFilterBtn.isSelected())
        {
            Prefilter pf=new Prefilter();
            if(pf.detectHub(document))
            {
                for(int i=0;i<dtm.getDataVector().size();i++)
                {
                    Vector v=(Vector)dtm.getDataVector().get(i);
                    if(i!=3)
                        v.set(2, false);
                    else v.set(2, true);
                }
                return "others";
            }
        }
        return category;
    }
    private void classifyFileBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classifyFileBtnActionPerformed
        JFileChooser jf=setCurrentDialog("urls");
        String fileName;
        String category;
        try
        {
            if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION)
            {
                fileName=jf.getSelectedFile().getPath();
                category=jf.getSelectedFile().getName().split(".txt")[0];
                if(fileName!=null &&globalTermVectors!=null &&documentList!=null&&!threadLock)
                {
                    JOptionPane.showMessageDialog(this, "Click to begin classifying web pages. Please wait...");
                    //resultDocuments=new GetCategory(globalTermVectors,documentList,singleSimilarityThreshold).getClassifiedDocumentListByUrlsFile(fileName,category);
                    //updateResultTable();
                    //JOptionPane.showMessageDialog(this, "Classification Completed!");
                    //mainFrame.setSelectedIndex(4);
                    byUrlThread=new ClassifyByFileThread(this,fileName,category);
                    byUrlThread.start();
                    auto=false;
                    initPause();
                }
                else if(threadLock)
                    JOptionPane.showMessageDialog(this, "There is a thread of classification executing now!");
                else JOptionPane.showMessageDialog(this, "Please initial or load Training Vectors first!");
            }
        }
        catch(Exception ex){}
    }//GEN-LAST:event_classifyFileBtnActionPerformed

    private void classifyDirectoryBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_classifyDirectoryBtnActionPerformed
        JFileChooser jf=setCurrentDialog(".");
        jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        String fileName=null;
        try
        {
            if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION)
            {
                fileName=jf.getSelectedFile().getPath();
                if(fileName!=null &&globalTermVectors!=null &&documentList!=null&&!threadLock)
                {
                    JOptionPane.showMessageDialog(this, "Click to begin classifying web pages. Please wait...");
                    //resultDocuments=new GetCategory(globalTermVectors,documentList,singleSimilarityThreshold).getClassifiedDocumentListByDirectory(fileName);
                    //updateResultTable();
                    //JOptionPane.showMessageDialog(this, "Classification Completed!");
                    //mainFrame.setSelectedIndex(4);                   
                    byDirectoryThread=new ClassifyByDirectoryThread(this,fileName);
                    byDirectoryThread.start();
                    auto=false;
                    initPause();
                }
                else if(threadLock)
                    JOptionPane.showMessageDialog(this, "There is a thread of classification executing now!");
                else JOptionPane.showMessageDialog(this, "Please initial or load Training Vectors first!");
            }
        }
        catch(Exception ex){}
    }//GEN-LAST:event_classifyDirectoryBtnActionPerformed

    private void realTimeClassifyBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_realTimeClassifyBtnActionPerformed
        JFileChooser jf=setCurrentDialog(".");
        jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        String fileName=null;
        try
        {
            if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION)
            {
                fileName=jf.getSelectedFile().getPath();
                if(fileName!=null &&globalTermVectors!=null &&documentList!=null&&!threadLock)
                {
                    JOptionPane.showMessageDialog(this, "Click to begin classifying web pages. Please wait...");
                    realtimeThread=new RealtimeClassifyThread(this,fileName);
                    realtimeThread.start();
                    auto=false;
                    initPause();
                }
                else if(threadLock)
                    JOptionPane.showMessageDialog(this, "There is a thread of classification executing now!");
                else JOptionPane.showMessageDialog(this, "Please initial or load Training Vectors first!");
            }
        }
        catch(Exception ex){}
    }//GEN-LAST:event_realTimeClassifyBtnActionPerformed

    private void initPause()
    {
        pause=true;
        pauseBtn.setText("Pause");
    }
    private void pauseBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pauseBtnActionPerformed
        try
        {         
            if(pause)
            {
                pause=!pause;
                if(realtimeThread!=null)
                    realtimeThread.pause();
                if(byDirectoryThread!=null)
                    byDirectoryThread.pause();
                if(byUrlThread!=null)
                    byUrlThread.pause();
                pauseBtn.setText("Continue");
            }
            else
            {
                pause=!pause;
                if(realtimeThread!=null)
                    realtimeThread.go();
                if(byDirectoryThread!=null)
                    byDirectoryThread.go();
                if(byUrlThread!=null)
                    byUrlThread.go();
                pauseBtn.setText("Pause");
            }
            
        }
        catch(Exception ex){ex.printStackTrace();}
    }//GEN-LAST:event_pauseBtnActionPerformed

    private void mainFrameStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_mainFrameStateChanged
        if(mainFrame.getSelectedIndex()==1)//click Feature Vectors tab
        {
            if(termTable.getRowSorter()==null) {
                termTable.setRowSorter(new TableRowSorter<TableModel>(termTable.getModel()));
                recordTable.setRowSorter(new TableRowSorter<TableModel>(recordTable.getModel()));
            }
        }
        if(mainFrame.getSelectedIndex()==2)//click Template tab
        {
            if(globalTermTable.getRowSorter()==null) {
                globalTermTable.setRowSorter(new TableRowSorter<TableModel>(globalTermTable.getModel()));
                globalRecordTable.setRowSorter(new TableRowSorter<TableModel>(globalRecordTable.getModel()));
            }
        }
        if(mainFrame.getSelectedIndex()==3)//click classification tab
        {
            if(kSimilarityTable.getRowSorter()==null) {
                kSimilarityTable.setRowSorter(new TableRowSorter<TableModel>(kSimilarityTable.getModel()));
                classTable.setRowSorter(new TableRowSorter<TableModel>(classTable.getModel()));
            }
        }
        if(mainFrame.getSelectedIndex()==4)//click results tab
        {
            if(resultTable.getRowSorter()==null) {
                resultTable.setRowSorter(new TableRowSorter<TableModel>(resultTable.getModel()));
            }
        }
}//GEN-LAST:event_mainFrameStateChanged

    private void categoryComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_categoryComboActionPerformed
        String category=categoryCombo.getSelectedItem().toString();
        DefaultTableModel tm=(DefaultTableModel)resultTable.getModel();
        if(tm.getRowCount()!=0) {
            Evaluation evaluation=new Evaluation(tm.getDataVector());
            double precision=evaluation.getPrecision(category);
            double recall=evaluation.getRecall(category);
            double f=evaluation.getF(category, 1.0);
            precisionText.setText(String.valueOf(precision));
            recallText.setText(String.valueOf(recall));
            fText.setText(String.valueOf(f));
        }
    }//GEN-LAST:event_categoryComboActionPerformed

    private void autoCalculateBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoCalculateBtnActionPerformed
        if(globalTermVectors!=null &&documentList!=null)
        {
            autoCalculate();
        }
        else JOptionPane.showMessageDialog(this, "Please initial or load Training Vectors first!");

}//GEN-LAST:event_autoCalculateBtnActionPerformed

    private void loadConfigBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadConfigBtnActionPerformed
        if(JOptionPane.showConfirmDialog(this, "Confirm to Load Parameters?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION) {
            initialParameters();
            JOptionPane.showMessageDialog(this, "Load Parameters Completed!");
        }
}//GEN-LAST:event_loadConfigBtnActionPerformed

    private void saveConfigBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveConfigBtnActionPerformed
        if(JOptionPane.showConfirmDialog(this, "Confirm to Save Parameters?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION) {
            saveConfigFile();
            JOptionPane.showMessageDialog(this, "Save Parameters Completed!");
        }
    }//GEN-LAST:event_saveConfigBtnActionPerformed

    private void saveConfigFile()
    {
        Config config=new Config("Parameters.props");
        singleSimilarityThreshold=Double.parseDouble(singleSimilarityThresholdText.getText());
        categorySimilarityThreshold=Double.parseDouble(categorySimilarityThresholdText.getText());
        lowFrequencyThreshold=Integer.parseInt(lowFrequencyThresholdText.getText());
        config.setValue("singleSimilarityThreshold", String.valueOf(singleSimilarityThreshold));
        config.setValue("categorySimilarityThreshold", String.valueOf(categorySimilarityThreshold));
        config.setValue("lowFrequencyThreshold", String.valueOf(lowFrequencyThreshold));
        config.setValue("aToPRatioThreshold", aToPRatioThresholdText.getText());
        config.setValue("aToInnerRatioThreshold", aToInnerRatioThresholdText.getText());
        config.saveFile();
    }

    private void resultTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_resultTableMouseClicked
        if(resultTable.getSelectedColumn()==0) {
            int rowIndex=resultTable.getSelectedRow();
            String title=resultTable.getValueAt(rowIndex, 0).toString();
            Config config=new Config("URLs.props");
            String url=config.getValue(title);
            resultUrlText.setText(url);
            System.out.println(title+" "+url);
        }
}//GEN-LAST:event_resultTableMouseClicked

    private void kResetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kResetBtnActionPerformed
        thresholdText.setText(String.valueOf(singleSimilarityThreshold));
        updateKSimilarityTable();
        kFilter();
}//GEN-LAST:event_kResetBtnActionPerformed

    private void kFilterBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kFilterBtnActionPerformed
        kFilter();
}//GEN-LAST:event_kFilterBtnActionPerformed

    private void globalTermResetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_globalTermResetBtnActionPerformed
        updateGlobalTermTable();
}//GEN-LAST:event_globalTermResetBtnActionPerformed

    private void loadGlobalVectorsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadGlobalVectorsBtnActionPerformed
        if(documentList!=null) {
            if(JOptionPane.showConfirmDialog(this, "Load Template may be time consuming, do you Continue?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION) {
                System.out.print("Load GlobalTermVectors: ");
                Long t1=System.currentTimeMillis();
                ReadIn ri=new ReadIn();
                globalTermVectors=ri.loadFullGlobalTermVectors(documentList); //100s
                Long t2=System.currentTimeMillis();
                System.out.println(t2-t1);
                if(globalTermVectors!=null) {
                    updateGlobalTermTable();
                }
                JOptionPane.showMessageDialog(this, "Load Template Completed!");
            }
        } else
            JOptionPane.showMessageDialog(this, "Please initial or load Training Vectors first!");
}//GEN-LAST:event_loadGlobalVectorsBtnActionPerformed

    private void saveGlobalVectorsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveGlobalVectorsBtnActionPerformed
        try {
            if(globalTermTable.getRowCount()!=0) {
                if(JOptionPane.showConfirmDialog(this, "Confirm to Save Template?","",JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION) {
                    DefaultTableModel tm=(DefaultTableModel)globalTermTable.getModel();
                    Vector vector=tm.getDataVector();
                    ArrayList<String> terms=new ArrayList<String>();
                    for(Object o:vector) {
                        Vector v=(Vector)o;
                        terms.add(v.get(0).toString());
                    }
                    for(int i=0;i<globalTermVectors.size();i++) {
                        if(!terms.contains(globalTermVectors.get(i).getName())) {
                            globalTermVectors.remove(i);
                            i--;
                        }
                    }
                    /*
                    //update document list's termvectors
                    for(Document d:documentList)
                    {
                        d.setTermVector(new GetTermVectors(globalTermVectors).getTermVectorsByDocument(d));
                    }
                     *
                     */
                    JOptionPane.showMessageDialog(this, "Save Template Completed!");
                }
            } else JOptionPane.showMessageDialog(this, "Template Null!");
        } catch(Exception e){}
}//GEN-LAST:event_saveGlobalVectorsBtnActionPerformed

    private void globalTermFilterButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_globalTermFilterButton1ActionPerformed
        try {
            DefaultTableModel tm=(DefaultTableModel)globalTermTable.getModel();
            Vector vector=tm.getDataVector();
            String expression=globalTermFilterCombo.getSelectedItem().toString().trim();
            String item=globalTermFilterCombo.getEditor().getItem().toString();
            int category=0;
            if(item.split("=")[0].equalsIgnoreCase("frequency"))
                category=1;
            else if(item.split("=")[0].equalsIgnoreCase("idf"))
                category=2;
            else if(item.split("=")[0].equalsIgnoreCase("ig"))
                category=3;
            double min=Double.parseDouble(expression.split("=")[1].split(":")[0]);
            double max=Double.parseDouble(expression.split("=")[1].split(":")[1]);
            deleteRowOfTermTable(vector,min,max,tm,category);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(this, "Filter Expression Format Error!");
        }
}//GEN-LAST:event_globalTermFilterButton1ActionPerformed

    private void globalTermTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_globalTermTableMouseClicked
        if(globalTermTable.getSelectedColumn()==0) {
            int rowIndex=globalTermTable.getSelectedRow();
            String term=globalTermTable.getValueAt(rowIndex, 0).toString();
            for(Term t:globalTermVectors) {
                if(t.getName().equals(term)) {
                    updateGlobalRecordTable(term);
                    break;
                }
            }
        }
}//GEN-LAST:event_globalTermTableMouseClicked

    private void termResetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_termResetBtnActionPerformed
        if(document!=null)
            updateTermTable(document);
}//GEN-LAST:event_termResetBtnActionPerformed

    private void termFilterButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_termFilterButtonActionPerformed
        try {
            DefaultTableModel tm=(DefaultTableModel)termTable.getModel();
            Vector vector=tm.getDataVector();
            String expression=termFilterCombo.getSelectedItem().toString().trim();
            String item=termFilterCombo.getEditor().getItem().toString();
            int category=0;
            if(item.split("=")[0].equalsIgnoreCase("frequency"))
                category=1;
            else if(item.split("=")[0].equalsIgnoreCase("idf"))
                category=2;
            else if(item.split("=")[0].equalsIgnoreCase("ig"))
                category=3;
            double min=Double.parseDouble(expression.split("=")[1].split(":")[0]);
            double max=Double.parseDouble(expression.split("=")[1].split(":")[1]);
            deleteRowOfTermTable(vector,min,max,tm,category);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(this, "Filter Expression Format Error!");
        }
}//GEN-LAST:event_termFilterButtonActionPerformed

    private void termTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_termTableMouseClicked
        if(termTable.getSelectedColumn()==0) {
            int rowIndex=termTable.getSelectedRow();
            String term=termTable.getValueAt(rowIndex, 0).toString();
            if(globalTermVectors!=null) {
                for(Term t:globalTermVectors) {
                    if(t.getName().equals(term)) {
                        updateRecordTable(t);
                        break;
                    } else {
                        ((DefaultTableModel)recordTable.getModel()).setRowCount(0);
                    }
                }
            }
        }
}//GEN-LAST:event_termTableMouseClicked

    private void loadVectorsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadVectorsBtnActionPerformed
        JFileChooser jf=setCurrentDialog("pages");
        String fileName=null;
        String name=null;
        Document d=null;
        if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION) {
            fileName=jf.getSelectedFile().getPath();
            name=jf.getSelectedFile().getName();
        }
        if(fileName!=null) {
            d=new GetHtml().getDocumentByFileName(fileName, globalTermVectors);
            termTitleText.setText(d.getTitle());
            document=d;
        }
        updateTermTable(d);
}//GEN-LAST:event_loadVectorsBtnActionPerformed

    private void updateUrlsBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateUrlsBtnActionPerformed
        JFileChooser jf=setCurrentDialog("urls");
        String fileName=null;
        String str=hrefText.getText();
        if(!str.equals("")) {
            if(jf.showDialog(this, "Update")==JFileChooser.APPROVE_OPTION) {
                fileName=jf.getSelectedFile().getPath();
                new WriteOut().writeStrAtEnd(str,fileName);
                JOptionPane.showMessageDialog(this, "Update URLs Completed!");
            }
        } else JOptionPane.showMessageDialog(this, "Hrefs Null!");
}//GEN-LAST:event_updateUrlsBtnActionPerformed

    private void getPageModelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getPageModelBtnActionPerformed
        String str=bodyText.getText();
        if(!str.equals("")) {
            Document d=new Document();
            GetTermVectors gtv=new GetTermVectors(globalTermVectors);
            document=d;
            d.setHtml(str);
            d.setTitle(titleText.getText());
            d.setTitleVector(gtv.getTitleVectors(titleText.getText(), ""));
            d.setTermVector(gtv.getTermVectorsByDocument(d));
            updateTermTable(d);
            JOptionPane.showMessageDialog(this, "Get Page Model Completed!");
        } else JOptionPane.showMessageDialog(this, "You have not load Document!");

    }//GEN-LAST:event_getPageModelBtnActionPerformed

    private void loadFeaturesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadFeaturesBtnActionPerformed
        try {
            Long t1=System.currentTimeMillis();
            String fileName=null;
            JFileChooser jf=setCurrentDialog("pages");
            if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION) {
                fileName=jf.getSelectedFile().getPath();
            }
            if(fileName!=null) {
                File f=new File(fileName);
                org.jsoup.nodes.Document d=Jsoup.parse(f, "GBK");
                titleText.setText(FetchContent.fetch(d.title()));
                keywordText.setText(d.getElementsByAttributeValue("name", "keywords").attr("content"));
                descriptionText.setText(d.getElementsByAttributeValue("name", "description").attr("content"));
                bodyText.setText(d.body().text());
                Elements es=d.getElementsByTag("a");
                for(Element e:es) {
                    anchorText.setText(anchorText.getText()+e.text()+"\r\n");
                    hrefText.setText(hrefText.getText()+e.attr("href")+"\r\n");
                }
                Elements eps=d.getElementsByTag("p");
                for(Element e:eps) {
                    paragraphText.setText(paragraphText.getText()+e.text()+"\r\n");
                }
            }
            System.out.println(System.currentTimeMillis()-t1);
        } catch(Exception e){}
}//GEN-LAST:event_loadFeaturesBtnActionPerformed

    private void loadHtmlBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loadHtmlBtnActionPerformed
        emptyText();
        String fileName=null;
        JFileChooser jf=setCurrentDialog("pages");
        if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION) {
            fileName=jf.getSelectedFile().getPath();
        }
        if(fileName!=null)
            bodyText.setText(new ReadIn().readStr(fileName));
}//GEN-LAST:event_loadHtmlBtnActionPerformed

    private void checkHTMLBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_checkHTMLBtnActionPerformed
        emptyText();
        String url=urlText.getText();
        String html=null;
        try {
            html=new GetHtml().getHtmlByUrl(url);
            bodyText.setText(html);
        } catch (Exception e) {
            e.printStackTrace();
        }
}//GEN-LAST:event_checkHTMLBtnActionPerformed

    private void getFeaturesBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_getFeaturesBtnActionPerformed
        try {
            Long t1=System.currentTimeMillis();
            emptyText();
            org.jsoup.nodes.Document page=Jsoup.connect(urlText.getText()).get();
            titleText.setText(FetchContent.fetch(page.title()));
            keywordText.setText(page.getElementsByAttributeValue("name", "keywords").attr("content"));
            descriptionText.setText(page.getElementsByAttributeValue("name", "description").attr("content"));
            bodyText.setText(page.body().text());
            Elements es=page.getElementsByTag("a");
            for(Element e:es) {
                anchorText.setText(anchorText.getText()+e.text()+"\r\n");
                hrefText.setText(hrefText.getText()+e.attr("href")+"\r\n");
            }
            Elements eps=page.getElementsByTag("p");
            for(Element e:eps) {
                paragraphText.setText(paragraphText.getText()+e.text()+"\r\n");
            }
            System.out.println(System.currentTimeMillis()-t1);
        } catch(Exception ex) {
            //ex.printStackTrace();
        }
}//GEN-LAST:event_getFeaturesBtnActionPerformed

    private void autoCalculateStopBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoCalculateStopBtnActionPerformed
        if(realtimeThread!=null)
        {
            auto=false;
            realtimeThread.stop=true;
            threadLock=false;
            pause=false;
            pauseBtn.setText("Pause");
            pauseBtn.setEnabled(false);
            progressBar.setValue(0);
            realtimeThread.pause();
        }
    }//GEN-LAST:event_autoCalculateStopBtnActionPerformed

    private void chooseDataSourceBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseDataSourceBtnActionPerformed
        JFileChooser jf=setCurrentDialog(".");
        jf.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        String fileName=null;
        if(jf.showDialog(this, "Load")==JFileChooser.APPROVE_OPTION) {
                fileName=jf.getSelectedFile().getPath();
                datasourceText.setText(fileName);
        }
    }//GEN-LAST:event_chooseDataSourceBtnActionPerformed

    private void termFilterBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_termFilterBtnActionPerformed
        double single=Double.parseDouble(singleSimilarityThresholdText.getText());
        double category=Double.parseDouble(categorySimilarityThresholdText.getText());
        if(termFilterBtn.isSelected())
        {
            singleSimilarityThresholdText.setText(String.valueOf(single*3.5));
            categorySimilarityThresholdText.setText(String.valueOf(category*3.5));
            saveConfigFile();
        }
        else
        {
            singleSimilarityThresholdText.setText(String.valueOf(single/3.5));
            categorySimilarityThresholdText.setText(String.valueOf(category/3.5));
            saveConfigFile();
        }
    }//GEN-LAST:event_termFilterBtnActionPerformed

    public void autoCalculate()
    {
        try
        {
            auto=true;
            if(auto&&!threadLock)
            {
                double targetFValue=Double.parseDouble(targetFValueText.getText());
                if(fText.getText().equals("")||fText.getText().equals("NaN"))
                    fText.setText("0.0");
                double currentFValue=Double.parseDouble(fText.getText());
                double delta=currentFValue-previousFValue;
                previousFValue=currentFValue;
                deltaText.setText(String.valueOf(delta));
                initialParameters();
                if(currentFValue<targetFValue)
                {
                    if(parametersCombo.getSelectedIndex()==0)
                    {
                        singleSimilarityThreshold+=Double.parseDouble(thresholdStep.getText());
                    }
                    else if(parametersCombo.getSelectedIndex()==1)
                    {
                        categorySimilarityThreshold+=Double.parseDouble(thresholdStep.getText());
                    }
                    else if(parametersCombo.getSelectedIndex()==2)
                    {
                        lowFrequencyThreshold+=Integer.parseInt(thresholdStep.getText());
                    }
                
                    Config config=new Config("Parameters.props");
                    config.setValue("singleSimilarityThreshold", String.valueOf(singleSimilarityThreshold));
                    config.setValue("categorySimilarityThreshold", String.valueOf(categorySimilarityThreshold));
                    config.setValue("lowFrequencyThreshold", String.valueOf(lowFrequencyThreshold));
                    config.saveFile();
                    currentFValueText.setText(String.valueOf(currentFValue));
                    realtimeThread=new RealtimeClassifyThread(this,datasourceText.getText());
                    threadLock=true;
                    realtimeThread.start();
                    initPause();
                }
                else
                {
                    JOptionPane.showMessageDialog(this, "Auto Calculate Parameters Completed!");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "There is a thread of classification executing now!");
            }
        }
        catch(Exception ex){JOptionPane.showMessageDialog(this, "Data Format Errors!");}
    }
    private void deleteRowOfTermTable(Vector vector,double min,double max,DefaultTableModel tm,int category)
    {
        for(int i=0;i<vector.size();i++)
         {
             Vector v=(Vector)vector.get(i);
             double frequency=Double.parseDouble(v.get(category).toString());
             if(frequency<min||frequency>max)
             {
                 tm.removeRow(i);
                 i--;            
             }
         }
        if(tm.equals(termTable.getModel()))
        {
            termCountText.setText(String.valueOf(tm.getRowCount()));
        }
        else if(tm.equals(globalTermTable.getModel()))
        {
            globalTermCountText.setText(String.valueOf(tm.getRowCount()));
            vector=tm.getDataVector();
            int termDocCount=0;
            for(int i=0;i<vector.size();i++)
            {
                 Vector v=(Vector)vector.get(i);
                 termDocCount+=Integer.parseInt(v.get(2).toString());//get idf
             }
            globalTermDocumentCountText.setText(String.valueOf(termDocCount));
        }
    }
    private void updateRecordTable(Term t)
    {
        DefaultTableModel tableModel=(DefaultTableModel)recordTable.getModel();
        tableModel.setRowCount(0);
        for(Record r:t.getRecordsList())
        {
            Object[] arr=new Object[3];
            arr[0]=r.getDocNum();//docNum
            arr[1]=r.getFrequency();//frequency
            arr[2]=r.getTFIDF();//tfidf
            tableModel.addRow(arr);
        }
        recordTable.invalidate();
    }

    private void updateResultTable()
    {
        DefaultTableModel tm=(DefaultTableModel)resultTable.getModel();
        tm.setRowCount(0);
        resultNumberText.setText(String.valueOf(resultDocuments.size()));
        for(Document d:resultDocuments)
        {
            Object[] arr=new Object[3];
            arr[0]=d.getTitle();
            arr[1]=d.getTrueCategory();
            arr[2]=d.getCategory();
            tm.addRow(arr);
        }
        resultTable.invalidate();
    }
    
    
  
   


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField aToInnerRatioThresholdText;
    public javax.swing.JTextField aToPRatioThresholdText;
    private javax.swing.JTextArea anchorText;
    private javax.swing.JButton autoCalculateBtn;
    private javax.swing.JButton autoCalculateStopBtn;
    public javax.swing.JTextArea bodyText;
    public javax.swing.JComboBox categoryCombo;
    private javax.swing.JTextField categorySimilarityThresholdText;
    private javax.swing.JButton checkHTMLBtn;
    private javax.swing.JButton chooseDataSourceBtn;
    private javax.swing.JTable classTable;
    public javax.swing.JButton classifyDirectoryBtn;
    public javax.swing.JButton classifyFileBtn;
    private javax.swing.JButton closeBtn;
    private javax.swing.JTextField currentFValueText;
    private javax.swing.JTextField datasourceText;
    private javax.swing.JTextField deltaText;
    private javax.swing.JTextArea descriptionText;
    public javax.swing.JTextField fText;
    private javax.swing.JButton getFeaturesBtn;
    private javax.swing.JButton getHtmlsBtn;
    private javax.swing.JButton getPageModelBtn;
    private javax.swing.JTable globalRecordTable;
    private javax.swing.JTextField globalTermCountText;
    private javax.swing.JTextField globalTermDocumentCountText;
    private javax.swing.JButton globalTermFilterButton1;
    private javax.swing.JComboBox globalTermFilterCombo;
    private javax.swing.JButton globalTermResetBtn;
    private javax.swing.JTable globalTermTable;
    private javax.swing.JTextArea hrefText;
    public javax.swing.JToggleButton hubFilterBtn;
    public javax.swing.JButton initialPageModelBtn;
    private javax.swing.JButton initialTrainingVectors;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JButton kFilterBtn;
    private javax.swing.JButton kResetBtn;
    private javax.swing.JTable kSimilarityTable;
    private javax.swing.JSpinner kSpinner;
    private javax.swing.JTextField kTitleText;
    private javax.swing.JTextArea keywordText;
    private javax.swing.JButton loadConfigBtn;
    private javax.swing.JButton loadFeaturesBtn;
    private javax.swing.JButton loadGlobalVectorsBtn;
    private javax.swing.JButton loadHtmlBtn;
    private javax.swing.JButton loadTrainingVectorsBtn;
    private javax.swing.JButton loadVectorsBtn;
    public javax.swing.JTextField lowFrequencyThresholdText;
    public javax.swing.JTabbedPane mainFrame;
    private javax.swing.JTextArea paragraphText;
    private javax.swing.JComboBox parametersCombo;
    public javax.swing.JButton pauseBtn;
    public javax.swing.JTextField precisionText;
    public javax.swing.JProgressBar progressBar;
    public javax.swing.JButton realTimeClassifyBtn;
    public javax.swing.JTextField recallText;
    private javax.swing.JTable recordTable;
    public javax.swing.JTextField resultNumberText;
    public javax.swing.JTable resultTable;
    public javax.swing.JTextField resultUrlText;
    private javax.swing.JButton saveConfigBtn;
    private javax.swing.JButton saveGlobalVectorsBtn;
    private javax.swing.JButton saveTrainingVectorsBtn;
    private javax.swing.JButton showCategoryBtn;
    private javax.swing.JTextField singleSimilarityThresholdText;
    private javax.swing.JTextField targetFValueText;
    private javax.swing.JTextField termCountText;
    public javax.swing.JToggleButton termFilterBtn;
    private javax.swing.JButton termFilterButton;
    private javax.swing.JComboBox termFilterCombo;
    private javax.swing.JButton termResetBtn;
    private javax.swing.JTable termTable;
    private javax.swing.JTextField termTitleText;
    private javax.swing.JTextField thresholdStep;
    private javax.swing.JTextField thresholdText;
    private javax.swing.JTextArea titleText;
    private javax.swing.JButton updateUrlsBtn;
    private javax.swing.JTextField urlText;
    // End of variables declaration//GEN-END:variables

}
