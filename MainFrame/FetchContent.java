package MainFrame;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FetchContent {

    private FetchContent()
    {

    }
    public static String fetch(String html)
    {
        try
        {
            String htmlStr=html;
            Pattern p_script;
            Matcher m_script;
            Pattern p_style;
            Matcher m_style;
            Pattern p_html;
            Matcher m_html;
            Pattern p_cont1;
            Matcher m_cont1;
            Pattern p_cont2;
            Matcher m_cont2;

            String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>";
            String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>";
            String regEx_html = "<[^>]+>";
            String regEx_cont1 = "[\\d+\\s*`~!@#$%^&*\\(\\)\\+=|{}':;',\\[\\].<>/?？，、、；·\"。~！@#￥%……&*（）——+|{}【】《》‘：”“’_-]";
            String regEx_cont2 = "[\\w[^\\W]*]";
            
            p_script = Pattern.compile(regEx_script,Pattern.CASE_INSENSITIVE);
            m_script = p_script.matcher(htmlStr);
            htmlStr = m_script.replaceAll("");//过滤script标签

            p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
            m_style = p_style.matcher(htmlStr);
            htmlStr = m_style.replaceAll(""); // 过滤style标签

            p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
            m_html = p_html.matcher(htmlStr);
            htmlStr = m_html.replaceAll(""); // 过滤html标签

            p_cont1 = Pattern.compile(regEx_cont1, Pattern.CASE_INSENSITIVE);
            m_cont1 = p_cont1.matcher(htmlStr);
            htmlStr = m_cont1.replaceAll(""); // 过滤其它标签

            p_cont2 = Pattern.compile(regEx_cont2, Pattern.CASE_INSENSITIVE);
            m_cont2 = p_cont2.matcher(htmlStr);
            htmlStr = m_cont2.replaceAll(""); // 过滤html标签

            return htmlStr;

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return html;
    }

}
