/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import javax.swing.SwingUtilities;

/**
 *
 * @author Administrator
 */
public class ClassifyByFileThread extends AbstractThread{

    public ClassifyByFileThread(WebClassifier wc,String fileName,String category)
    {
        super(wc,fileName,category);
    }

    public void run()
    {
        try
        {
            init();
            initFile();
            GetCategory getC=new GetCategory(wc.globalTermVectors,wc.documentList);
            String category;
            double similarity;
            for(String s:urlList)
            {
                if(stop)
                    break;
                value++;
                Document d=getC.getClassifiedDocumentByUrl(s);          
                if(d!=null)
                {
                    String title=d.getTitle();
                    d.setTrueCategory(fileCategoryName);
                    category="";
                    similarity=0.0;
                    if(wc.hubFilterBtn.isSelected())
                    {
                        Prefilter pf=new Prefilter();
                        if(pf.detectHub(d))
                        {
                            category="others";
                            similarity=0.0;
                        }
                    }
                    if(!category.equals("others"))
                    {
                        category=d.getCategory();
                        similarity=d.getSimilarity();
                    }
                    System.out.println(title+" Completed!"+" "+value+"/"+total+" "+category);
                    wc.resultUrlText.setText(title+" Completed!");
                    Object[] arr=new Object[4];
                    arr[0]=title;
                    arr[1]=fileCategoryName;
                    arr[2]=category;
                    arr[3]=similarity;
                    tm.addRow(arr);
                    wc.resultNumberText.setText(String.valueOf(value));
                    SwingUtilities.invokeLater(target);
                }
            }         
        }
        catch(Exception ex){System.out.println("Classification by File Errors!");}
        finally
        {
            noticeCompleted("Classification By File Completed!");
        }
    }

}
