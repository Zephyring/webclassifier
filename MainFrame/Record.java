/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

/**
 *
 * @author Administrator
 */
public class Record {

    private String docNum;
    private int frequency;
    private Double tfidf;
    private String trueCategory;

    public Record(String docNum, String trueCategory,int frequency)
    {
        this.docNum=docNum;
        this.trueCategory=trueCategory;
        this.frequency=frequency;
    }

    public Record()
    {
        
    }

    public String getDocNum()
    {
        return this.docNum;
    }
    public void setDocNum(String docNum)
    {
        this.docNum=docNum;
    }
    public String getTrueCategory()
    {
        return this.trueCategory;
    }
    public void setTrueCategory(String trueCategory)
    {
        this.trueCategory=trueCategory;
    }

    public int getFrequency()
    {
        return this.frequency;
    }
    public void setFrequency(int frequency)
    {
        this.frequency=frequency;
    }
    public void addFrequency()
    {
        this.frequency+=1;
    }
    public void addFrequency(int delta)
    {
        this.frequency+=delta;
    }

    public Double getTFIDF()
    {
        return this.tfidf;
    }
    public void setTFIDF(Double tfidf)
    {
        this.tfidf=tfidf;
    }

}
