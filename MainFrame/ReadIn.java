/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ReadIn {
    private ArrayList<Term> globalTermVectors=null;

    public ReadIn()
    {

    }
    public ReadIn(ArrayList<Term> globalTermVectors)
    {
        this.globalTermVectors=globalTermVectors;
    }
    
    public ArrayList<String> readStopwords(String fileName)
    {
        ArrayList<String> stopwords=new ArrayList<String>();
        try
        {         
            BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"GBK"));
            String line;
            while((line=br.readLine())!=null)
            {
                stopwords.add(line);
            }
            br.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return stopwords;
    }

    public ArrayList<String> readUrlList(String fileName)
    {
        ArrayList<String> urlList=new ArrayList<String>();
        try
        {
            BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(fileName),"GBK"));
            String line;
            while((line=br.readLine())!=null)
            {
                if(line.startsWith("http:"))
                    urlList.add(line);
            }
            br.close();
        }
        catch(Exception ex)
        {
        }
        return urlList;
    }

    public String readStr(String fileName)
    {
        StringBuffer sb;
        try
        {
            sb=new StringBuffer();
            FileInputStream fis=new FileInputStream(fileName);
            InputStreamReader isr=new InputStreamReader(fis,"GBK");
            BufferedReader br=new BufferedReader(isr);
            String line;
            while((line=br.readLine())!=null)
            {
                sb.append(line);
            }
            sb.trimToSize();
            br.close();
            isr.close();
            fis.close();

            return sb.toString();
        }
        catch(Exception ex){}
        finally
        {
            sb=null;
        }

        return null;
    }

    public ArrayList<Document> getDocumentListByDirectory(String dirPath)
    {
        ArrayList<Document> documentList=new ArrayList<Document>();
        File dir=new File(dirPath);
        File[] fs=dir.listFiles();
        for(File f:fs)
        {
            if(f.isDirectory())
            {
                ArrayList<Document> temp=this.getDocumentsByDirectory(f);
                documentList.addAll(temp);
            }
            else
            {
                ArrayList<Document> temp=this.getDocumentsByDirectory(dir);
                documentList.addAll(temp);
                break;
            }
        }
        return documentList;
    }
    private ArrayList<Document> getDocumentsByDirectory(File f)
    {
        //f is a directory
        try
        {
            if(f.isDirectory())
            {
                ArrayList<Document> documentList=new ArrayList<Document>();
                File page=new File(f.getPath()+"/");
                File[] pages=page.listFiles();
                String trueCategory=f.getName();
                int i=0;
                int total=pages.length;
                Document document;
                String filePath;
                String title;
                for(File p:pages)
                {
                    i++;
                    document=new Document();
                    filePath=p.getPath();
                    title=this.getTitles(p.getName());
                    if(title!=null)
                    {
                        document.setTrueCategory(trueCategory);
                        document.setFilePath(filePath);
                        document.setTitle(title);
                        document.setTermVector(new GetTermVectors(globalTermVectors).getTermVectorsByDocumentWithFilePath(document));
                        document.setTitleVector(new GetTermVectors().getTitleVectors(title, trueCategory));
                        documentList.add(document);
                        System.out.println(title+" Ready!"+"  "+i+"/"+total);
                    }
                }
                
                return documentList;
            }
        }
        catch(Exception ex){}
        return null;
    }

    public ArrayList<String> readTitles(String dirPath)
    {
        ArrayList<String> titles=new ArrayList<String>();
        File[] fs=new File(dirPath).listFiles();
        for(File f:fs)
        {
            titles.add(f.getName().split(".txt")[0]);
        }
        return titles;
    }
    public String getTitles(String fileName)
    {
        String title;
        try
        {
            title=fileName.split(".txt")[0];
            return title;
        }
        catch(Exception e){}

        return null;
    }

    public ArrayList<Term> loadGlobalTermVectorsByDatabase()
    {
        //time consuming!!!
        ArrayList<Term> globalTermVector=new ArrayList<Term>();
        ConnectSql conn=new ConnectSql();
        ResultSet rs=conn.query("select name,ig,chiInfo,chiDown,chiYellow from globalTermVectors");
        try
        {
            while(rs.next())
            {
                String name=rs.getString(1);
                String ig=rs.getString(2);
                String chiInfo=rs.getString(3);
                String chiDown=rs.getString(4);
                String chiYellow=rs.getString(5);
                Term t=new Term();
                t.setName(name);
                t.setIG(Double.parseDouble(ig));
                t.setCHIInfo(Double.parseDouble(chiInfo));
                t.setCHIDown(Double.parseDouble(chiDown));
                t.setCHIYellow(Double.parseDouble(chiYellow));
       
                ResultSet records=conn.query("select docNum,frequency,tfidf from globalTermVectorsRecords where name='"+name+"';");
                while(records.next())
                {
                    Record r=new Record();
                    String docNum=records.getString(1);
                    String frequency=records.getString(2);
                    String tfidf=records.getString(3);
                    r.setDocNum(docNum);
                    r.setFrequency(Integer.parseInt(frequency));
                    r.setTFIDF(Double.parseDouble(tfidf));
                    t.addRecord(r);
                }
                globalTermVector.add(t);
            }
        }
        catch(Exception e){System.out.println("Load GlobalTermVectors Errors!");}
        return globalTermVector;
    }

    public ArrayList<Term> loadGlobalTermVectorsWithIGByDocumentList()
    {
        ArrayList<Document> documentList=this.loadDocumentListByDatabase();
        return new GetTermVectors().getGlobalTermVectorsWithIG(documentList);
    }
    public ArrayList<Term> loadGlobalTermVectorsWithIGByDocumentList(ArrayList<Document> documentList)
    {
        return new GetTermVectors().getGlobalTermVectorsWithIG(documentList);
    }

    public ArrayList<Term> loadSimpleGlobalTermVectorsByDocumentList(ArrayList<Document> documentList)
    {
        return new GetTermVectors().getGlobalTermVectors(documentList);
    }
    public ArrayList<Term> loadGlobalTermVectorsWithCHIByDocumentList(ArrayList<Document> documentList)
    {
        return new GetTermVectors().getGlobalTermVectorsWithCHI(documentList);
    }
    public ArrayList<Term> loadFullGlobalTermVectors(ArrayList<Document> documentList)
    {
        return new GetTermVectors().getFullGlobalTermVectors(documentList);
    }

    public ArrayList<Document> loadDocumentListByDatabase()
    {
        ArrayList<Document> documentList=new ArrayList<Document>();
        ConnectSql conn=new ConnectSql();
        ResultSet rs=conn.query("select category, title from documents");
        try
        {
            while(rs.next())
            {
                Document d=new Document();
                String category=rs.getString(1);
                String title=rs.getString(2);
                d.setTrueCategory(category);
                d.setTitle(title);
                //load termVectors
                ResultSet terms=conn.query("select name, frequency from globalTermVectorsRecords where docNum='"+title+"';");
                ArrayList<Term> termVectors=new ArrayList<Term>();
                while(terms.next())
                {
                    Term t=new Term();
                    t.setName(terms.getString(1));
                    Record r=new Record();
                    r.setDocNum(title);
                    r.setTrueCategory(category);
                    r.setFrequency(Integer.parseInt(terms.getString(2)));
                    t.addRecord(r);
                    termVectors.add(t);
                }
                d.setTermVector(termVectors);
                //load titleVectors
                ResultSet titles=conn.query("select name, frequency from titleVectors where docNum='"+title+"';");
                ArrayList<Term> titleVectors=new ArrayList<Term>();
                while(titles.next())
                {
                    Term t=new Term();
                    t.setName(titles.getString(1));
                    Record r=new Record();
                    r.setDocNum(title);
                    r.setTrueCategory(category);
                    r.setFrequency(Integer.parseInt(titles.getString(2)));
                    t.addRecord(r);
                    titleVectors.add(t);
                }
                d.setTitleVector(titleVectors);
                
                documentList.add(d);
            }
            conn.getConnection().close();
            return documentList;
        }
        catch(Exception ex){System.out.println("Load DocumentList Errors!");}
        return null;
    }

    

    






}
