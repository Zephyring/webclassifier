/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrator
 */
public abstract class AbstractThread extends Thread{
    Runnable target;
    int value;
    int total;
    WebClassifier wc;
    String fileName;
    String fileCategoryName;
    public volatile Boolean stop=false;
    DefaultTableModel tm;
    File[] fs;
    ReadIn ri;
    ArrayList<String> urlList;

    public AbstractThread(WebClassifier wcc, String fileNamee)
    {
        wc=wcc;
        fileName=fileNamee;
        target=new Runnable()
        {
            public void run()
            {
                wc.progressBar.setValue(value);
            }
        };
    }
    public AbstractThread(WebClassifier wcc, String fileNamee,String category)
    {
        wc=wcc;
        fileName=fileNamee;
        fileCategoryName=category;
        target=new Runnable()
        {
            public void run()
            {
                wc.progressBar.setValue(value);
            }
        };
    }

    public  abstract void run();

    protected void init()
    {
        if(!wc.auto)
            wc.mainFrame.setSelectedIndex(4);
        wc.pauseBtn.setEnabled(true);
        wc.threadLock=true;
        tm=(DefaultTableModel)wc.resultTable.getModel();
        tm.setRowCount(0);
        wc.resultNumberText.setText("");        
    }

    protected void initDirectory()
    {
        File dir=new File(fileName);
        fs=dir.listFiles();
        ri=new ReadIn();
        getTotalFileCountByDirectory();
        wc.progressBar.setMaximum(total);
    }

    protected void initFile()
    {
        ri=new ReadIn();
        urlList=ri.readUrlList(fileName);
        getTotalFileCountByFile();
        wc.progressBar.setMaximum(total);
    }
    protected void getTotalFileCountByDirectory()
    {
        File dir=new File(fileName);
        File[] dirs=dir.listFiles();
        total=0;
        for(File f:dirs)
        {
            if(f.isDirectory())
            {
                File page=new File(f.getPath()+"/");
                File[] pages=page.listFiles();
                total+=pages.length;
            }
        }
    }
    protected void getTotalFileCountByFile()
    {
        total=urlList.size();
    }

    protected void noticeCompleted(String notice)
    {
        if(!wc.auto)
            JOptionPane.showMessageDialog(wc, notice);
        wc.pauseBtn.setEnabled(false);
        wc.threadLock=false;
        if(wc.auto)
        {
            String category=wc.categoryCombo.getSelectedItem().toString();
            if(tm.getRowCount()!=0)
            {
                Evaluation evaluation=new Evaluation(tm.getDataVector());
                double precision=evaluation.getPrecision(category);
                double recall=evaluation.getRecall(category);
                double f=evaluation.getF(category, 1.0);
                wc.precisionText.setText(String.valueOf(precision));
                wc.recallText.setText(String.valueOf(recall));
                wc.fText.setText(String.valueOf(f));
            }
            wc.autoCalculate();
        }
    }

    protected void pause()
    {
        try
        {
            synchronized(this){
                this.suspend();
            }
        }
        catch(Exception ex){ex.printStackTrace();}
    }

    protected void go()
    {
        try
        {
            synchronized(this)
            {
                this.resume();
            }
        }catch(Exception ex){ex.printStackTrace();}
    }

}
