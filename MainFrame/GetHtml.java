/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.util.ArrayList;
import org.jsoup.Jsoup;

/**
 *
 * @author Administrator
 */
public class GetHtml {

    public String getHtmlByUrl(String url)
    {       
        String outerHtml=null;
        try
        {
            outerHtml=Jsoup.connect(url).get().outerHtml();
            return outerHtml;
        }
        catch(Exception e){}
        return null;
    }

    public org.jsoup.nodes.Document getJsoupDocumentByUrl(String url)
    {
        org.jsoup.nodes.Document document=null;
        try
        {
            document=Jsoup.connect(url).get();
            return document;
        }
        catch(Exception e){}
        return null;
    }

    public Document getDocumentByUrl(String url,ArrayList<Term> globalTermVectors)
    {
        try
        {
            Document document=new Document();
            GetTermVectors gtv=new GetTermVectors(globalTermVectors);
            org.jsoup.nodes.Document dd=this.getJsoupDocumentByUrl(url);
            String html=dd.outerHtml();
            String title=null;
            title=FetchContent.fetch(dd.title());
            if(html!=null)
            {
                document.setHtml(html);
                if(title!=null)
                    document.setTitle(title);
                document.setTitleVector(gtv.getTitleVectors(title, ""));
                document.setTermVector(gtv.getTermVectorsByDocument(document));
            }
            return document;
        }
        catch(Exception e){}
        return null;
    }

    public Document getDocumentByHtml(String html,ArrayList<Term> globalTermVectors)
    {
        Document d=new Document();
        d.setHtml(html);
        d.setTitle(this.getTitleByHtml(html));
        d.setTermVector(new GetTermVectors(globalTermVectors).getTermVectorsByDocument(d));
        return d;
    }

    public Document getDocumentByFileName(String fileName,ArrayList<Term> globalTermVectors)
    {
        String html=new ReadIn().readStr(fileName);
        return this.getDocumentByHtml(html, globalTermVectors);
    }

    public String getTitleByUrl(String url)
    {
        String title=null;
        try
        {
            org.jsoup.nodes.Document d=Jsoup.connect(url).get();
            title=d.title();
            return title;
        }
        catch(Exception e)
        {

        }
        return null;
    }

    public String getTitleByFileName(String fileName)
    {
        String title=fileName.split(".txt")[0];
        return title;
    }

    public String getTitleByHtml(String html)
    {
        String title=Jsoup.parse(html).title();
        return title;
    }

}
