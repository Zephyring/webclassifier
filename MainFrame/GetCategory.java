/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class GetCategory {

    private ArrayList<Term> globalTermVectors=null;
    private ArrayList<Document> documentList=null;
    private Double singleSimilarityThreshold=0.0;
    private Double categorySimilarityThreshold=0.0;


    public GetCategory(ArrayList<Term> globalTermVectors, ArrayList<Document> documentList)
    {
        this.globalTermVectors=globalTermVectors;
        this.documentList=documentList;
        Config config=new Config("Parameters.props");
        singleSimilarityThreshold=Double.parseDouble(config.getValue("singleSimilarityThreshold"));
        categorySimilarityThreshold=Double.parseDouble(config.getValue("categorySimilarityThreshold"));
    }

    public String getCategoryByUrl(String url)
    {
        Document document=new GetHtml().getDocumentByUrl(url, globalTermVectors);
        return this.calculateCategory(document, singleSimilarityThreshold);
    }
    public Document getClassifiedDocumentByUrl(String url)
    {
        try
        {
            Document document=new GetHtml().getDocumentByUrl(url, globalTermVectors);
            document=calculateCategoryByDocument(document, singleSimilarityThreshold);
            return document;
        }
        catch(Exception ex){}
        return null;
        
    }

    public String getCategoryByHtml(String html)
    {
        Document document=new GetHtml().getDocumentByHtml(html, globalTermVectors);
        return this.calculateCategory(document, singleSimilarityThreshold);
    }
    public Document getClassifiedDocumentByHtml(String html)
    {
        Document document=new GetHtml().getDocumentByHtml(html, globalTermVectors);
        document.setCategory(this.calculateCategory(document, singleSimilarityThreshold));
        return document;
    }

    public String getCategoryByFileName(String fileName)
    {
        Document document=new GetHtml().getDocumentByFileName(fileName, globalTermVectors);
        return this.calculateCategory(document, singleSimilarityThreshold);
    }
    public Document getClassifiedDocumentByFileName(String fileName)
    {
        Document document=new GetHtml().getDocumentByFileName(fileName, globalTermVectors);
        document.setCategory(this.calculateCategory(document, singleSimilarityThreshold));
        return document;
    }

    public String getCategoryByDocument(Document document)
    {
        return this.calculateCategory(document, singleSimilarityThreshold);
    }
    public Document getClassifiedDocumentByDocument(Document document)
    {
        return this.calculateCategoryByDocument(document, singleSimilarityThreshold);
    }

    public ArrayList<Document> getClassifiedDocumentListByUrlsFile(String fileName,String category)
    {
        try
        {
            ArrayList<String> urlList=new ReadIn().readUrlList(fileName);
            ArrayList<Document> documents=new ArrayList<Document>();
            System.out.println("Total Number: "+urlList.size());
            int i=0;
            int total=urlList.size();
            for(String s:urlList)
            {
                i++;
                Document d=this.getClassifiedDocumentByUrl(s);    
                if(d!=null)
                {
                    d.setTrueCategory(category);
                    documents.add(d);
                    System.out.println(d.getTitle()+" Completed!"+" "+i+"/"+total);
                }
            }
            return documents;
        }
        catch(Exception ex){}
        return null;
    }

    public ArrayList<Document> getClassifiedDocumentListByDirectory(String dirName)
    {
        try
        {
            ArrayList<Document> documents=new ReadIn(globalTermVectors).getDocumentListByDirectory(dirName);
            System.out.println("Total Number: "+documents.size());
            int i=0;
            int total=documents.size();
            for(Document d:documents)
            {
                i++;
                if(d!=null)
                {
                    d=this.getClassifiedDocumentByDocument(d);
                    System.out.println(d.getTitle()+" Completed!"+" "+i+"/"+total);
                }
            }
            return documents;
        }
        catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public void realTimeClassifyDocumentListByDirectory(String dirName)
    {
        //has been disposed
        try
        {
            File dir=new File(dirName);
            File[] fs=dir.listFiles();
            ReadIn ri=new ReadIn();
            for(File f:fs)
            {
                if(f.isDirectory())
                {
                    File page=new File(f.getPath()+"/");
                    File[] pages=page.listFiles();
                    String trueCategory=f.getName();
                    int i=0;
                    int total=pages.length;
                    String title;
                    ArrayList<Term> vectors;
                    ArrayList<Term> titleVectors;
                    Document d=new Document();
                    GetTermVectors gtv=new GetTermVectors(globalTermVectors);
                    for(File p:pages)
                    {
                        i++;            
                        title=ri.getTitles(p.getName());
                        if(title!=null)
                        {
                            vectors=gtv.getTermVectorsByDocument(ri.readStr(p.getPath()),title,trueCategory);
                            titleVectors=gtv.getTitleVectors(title, trueCategory);
                            d.setTitle(title);
                            d.setTitleVector(titleVectors);
                            d.setFilePath(p.getPath());
                            d.setTrueCategory(trueCategory);
                            d.setTermVector(vectors);
                            System.out.println(title+" Ready!"+"  "+i+"/"+total);
                            System.out.println(title+" Completed!"+" "+i+"/"+total+" "+this.getCategoryByDocument(d));
                        }
                        
                    }
                }
            }
        }
        catch(Exception ex){ex.printStackTrace();}
    }

    private String calculateCategory(Document document, Double threshold)
    {
        String category;
        documentList=new GetSimilarity().getSimilarity(document, documentList);
        double informationSum=0.0;
        double downloadSum=0.0;
        double yellowSum=0.0;
        for(Document d:documentList)
        {
            if(d.getSimilarity()>=threshold)
            {
                if(d.getTrueCategory().equals("information"))
                    informationSum+=d.getSimilarity();
                else if(d.getTrueCategory().equals("download"))
                    downloadSum+=d.getSimilarity();
                else if(d.getTrueCategory().equals("yellow"))
                    yellowSum+=d.getSimilarity();
            }
        }

        Object[] n=weakFilter(informationSum,downloadSum,yellowSum);
        if((Integer)n[0]==1)
            category="information";
        else if((Integer)n[0]==2)
            category="download";
        else if((Integer)n[0]==3)
            category="yellow";
        else category="others";
        return category;
    }

    private Document calculateCategoryByDocument(Document document, Double threshold)
    {
        String category;
        documentList=new GetSimilarity().getSimilarity(document, documentList);
        double informationSum=0.0;
        double downloadSum=0.0;
        double yellowSum=0.0;
        for(Document d:documentList)
        {
            if(d.getSimilarity()>=threshold)
            {
                if(d.getTrueCategory().equals("information"))
                    informationSum+=d.getSimilarity();
                else if(d.getTrueCategory().equals("download"))
                    downloadSum+=d.getSimilarity();
                else if(d.getTrueCategory().equals("yellow"))
                    yellowSum+=d.getSimilarity();
            }
        }

        Object[] n=weakFilter(informationSum,downloadSum,yellowSum);
        if((Integer)n[0]==1)
        {
            category="information";
        }
        else if((Integer)n[0]==2)
        {
            category="download";
        }
        else if((Integer)n[0]==3)
        {
            category="yellow";
        }
        else
        {
            category="others";
        }
        document.setCategory(category);
        document.setSimilarity((Double)n[1]);
        document.setScore((Double)n[2]);
        document.sInfo=informationSum;
        document.sDown=downloadSum;
        document.sYellow=yellowSum;
        return document;
    }

    private Object[] weakFilter(double a,double b,double c)
    {
        //core algorithm to indicate which category it is
        /*
        if(a<categorySimilarityThreshold)
            a=0.0;
        return a;
         *
         */
        Object[] n=new Object[3];
        n[0]=0;
        n[1]=0.0;
        n[2]=0.0;
        double similarity=getMax(a,b,c);
        

        if(similarity!=0)
        {
            double score=0.0;
            if(findMax(a,b,c)==1)
            {
                score=a/(b*c);
            }
            else if(findMax(a,b,c)==2)
            {
                score=b/(a*c);
            }
            else if(findMax(a,b,c)==3)
            {
                score=c/(a*b);
            }
            if(score>0)
            {
                n[0]=findMax(a,b,c);
                n[1]=similarity;
                n[2]=score;
            }

            if(similarity<categorySimilarityThreshold)
            {
                n[0]=0;
                n[1]=0.0;
            }
        }
        return n;
    }

    private int findMax(Double a, Double b, Double c)
    {
        int n=0;
        if(a>b&&a>c)
            n=1;
        else if(b>a&&b>c)
            n=2;
        else if(c>a&&c>b)
            n=3;
        return n;
    }

    private double getMax(double a, double b, double c)
    {
        double max=0.0;
        switch(findMax(a,b,c))
        {
            case 0:break;
            case 1:max=a;break;
            case 2:max=b;break;
            case 3:max=c;break;
        }
        return max;
    }

}
