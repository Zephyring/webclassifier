/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.util.Vector;

/**
 *
 * @author Administrator
 */
public class Evaluation {

    private Vector v=null;

    public Evaluation(Vector v)
    {
        this.v=v;
    }

    public Double getPrecision(String category)
    {
        try
        {
            Double precision=0.0;
            int a=0;
            int b=0;
            for(Object o:v)
            {
                Vector vv=(Vector)o;
                if(vv.get(2).equals(category)&&vv.get(1).equals(category))
                    a+=1;
                if(vv.get(2).equals(category)&&(!vv.get(1).equals(category)))
                    b+=1;
            }
            precision=(double)a/(double)(a+b);
            return precision;
        }
        catch(Exception ex){}
        return 0.0;
    }

    public Double getRecall(String category)
    {
        try
        {
            Double recall=0.0;
            int a=0;
            int c=0;
            for(Object o:v)
            {
                Vector vv=(Vector)o;
                if(vv.get(2).equals(category)&&vv.get(1).equals(category))
                    a+=1;
                if((!vv.get(2).equals(category))&&vv.get(1).equals(category))
                    c+=1;
            }
            recall=(double)a/(double)(a+c);
            return recall;
        }
        catch(Exception ex){}
        return 0.0;
    }

    public Double getF(String category, Double beta)
    {
        try
        {
            Double f=0.0;
            Double precision=this.getPrecision(category);
            Double recall=this.getRecall(category);
            f=(beta*beta+1)*precision*recall/(beta*beta*precision+recall);
            return f;
        }
        catch(Exception e){}
        return 0.0;
    }

}
