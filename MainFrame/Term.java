/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Term {

    private String name;
    private ArrayList<Record> recordsList=new ArrayList<Record>();
    private double ig;
    private double chiInfo;
    private double chiDown;
    private double chiYellow;

    public Term()
    {

    }

    public Term(String name, Record record)
    {
        this.name=name;
        this.recordsList.add(record);
    }
    
    public Term(String name)
    {
        this.name=name;
    }

    public String getName(){return this.name;}
    public void setName(String name){this.name=name;}
    public ArrayList<Record> getRecordsList(){return this.recordsList;}
    public void addRecord(Record record){this.recordsList.add(record);}
    public double getIG(){return this.ig;}
    public void setIG(double ig){this.ig=ig;}
    public double getCHIInfo(){return this.chiInfo;}
    public void setCHIInfo(double chiInfo){this.chiInfo=chiInfo;}
    public double getCHIDown(){return this.chiDown;}
    public void setCHIDown(double chiDown){this.chiDown=chiDown;}
    public double getCHIYellow(){return this.chiYellow;}
    public void setCHIYellow(double chiYellow){this.chiYellow=chiYellow;}
}
