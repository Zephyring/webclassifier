package MainFrame;
import ICTCLAS.I3S.AC.ICTCLAS50;
  
public class DivideChinese {

    //this method uses ICTCLAS50 to divide chinese word
    private static ICTCLAS50 test=null;

    private DivideChinese()
    {

    }

    private static ICTCLAS50 getInstance()
    {
        if(test==null)
        {
            try
            {
                test=new ICTCLAS50();
                String argu = ".";
                if (test.ICTCLAS_Init(argu.getBytes("GBK")) == false)
                {
                    System.out.println("Fail to init ICTCLAS");
                }
                else
                {
                    System.out.println("Initial ICTCLAS Completed!");
                }
                return test;
            }
            catch(Exception ex){System.out.println("Fail to init ICTCLAS");}
            return test;
        }
        else return test;
    }

    public static String divideByICTCLAS(String word)
    {
        try
        {
            test=getInstance();
            String input=word;
            byte[] nativeByte =test.ICTCLAS_ParagraphProcess(input.getBytes("GBK"), 0, 1);
            String nativeStr=new String(nativeByte,0,nativeByte.length,"GBK");
            //test.ICTCLAS_Exit();
            return nativeStr;

        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

}
