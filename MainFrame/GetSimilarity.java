/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class GetSimilarity {

    public ArrayList<Document> getSimilarity(Document document,ArrayList<Document> documentList)
    {
        for(Document d:documentList)
        {          
            double similar=0.0;
            double termsSimilar=calculateTermSimilar(document,d);
            double titleSimilar=calculateTitleSimilar(document,d);
            similar+=termsSimilar;
            similar+=titleSimilar;
            d.setSimilarity(similar);
        }
        return documentList;
    }

    private double calculateTermSimilar(Document testD,Document d)
    {
        double similar=0.0;
        ArrayList<Term> testTerms=testD.getTermVector();
        ArrayList<Term> terms=d.getTermVector();
        similar=calculateSimilar(testTerms,terms);
        return similar;
    }

    private double calculateTitleSimilar(Document testD, Document d)
    {       
        double similar=0.0;        
        ArrayList<Term> testTitle=testD.getTitleVector();
        ArrayList<Term> title=d.getTitleVector();
        similar=calculateSimilar(testTitle,title);
        return similar;
    }

    private double calculateSimilar(ArrayList<Term> testT, ArrayList<Term> term2)
    {
        double similar=0.0;
        int ttSquare=0;
        int tSquare=0;
        int sum=0;
        for(Term tt:testT)
        {
            for(Term t:term2)//match existing terms in one document of training set
            {
                if(t.getName().equals(tt.getName()))
                {
                    //calculate the sum of tf
                    sum+=t.getRecordsList().get(0).getFrequency()*tt.getRecordsList().get(0).getFrequency();
                }
                tSquare+=t.getRecordsList().get(0).getFrequency()*t.getRecordsList().get(0).getFrequency();
            }
            ttSquare+=tt.getRecordsList().get(0).getFrequency()*tt.getRecordsList().get(0).getFrequency();
        }
        double ttNorm=Math.sqrt((double)ttSquare);
        double tNorm=Math.sqrt((double)tSquare);
        if(tNorm!=0.0)
               similar=(double)sum/(ttNorm*tNorm);
        return similar;
    }
}
