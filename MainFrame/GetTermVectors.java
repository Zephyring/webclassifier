/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 * get terms and frequencies by a specific url address
 */
public class GetTermVectors {

    private ArrayList<Document> documentList=null;
    private ArrayList<Term> globalTermsVectors=null;

    public ArrayList<Document> getDocumentList(){return this.documentList;}

    public GetTermVectors()
    {

    }
    public GetTermVectors(ArrayList<Term> globalTermVectors)
    {
        this.globalTermsVectors=globalTermVectors;
    }
    

    public ArrayList<Term> getTermVectorsByDocument(Document d)
    {
        try
        {
            String html=d.getHtml();
            String docNum=d.getTitle();
            String trueCategory=d.getTrueCategory();
            String content=FetchContent.fetch(html);//get Chinese words
            String wordsStr=DivideChinese.divideByICTCLAS(content);//divide into words
            String[] allTerms=this.getAllTerms(wordsStr);//get all terms
            ArrayList<String> noun=this.getTermsBySplitter(allTerms, "/n");//get noun
            ArrayList<String> verb=this.getTermsBySplitter(allTerms, "/v");//get verb
            ArrayList<String> temp=this.mixArrayList(noun, verb);//mix noun+verb
            ArrayList<String> filteredTerms=this.filterByStopWords(temp);//filter out stop words
            if(globalTermsVectors!=null&&WebClassifier.getInstance().termFilterBtn.isSelected())
                filteredTerms=this.filterByGlobalTerms(filteredTerms, globalTermsVectors);
            return new Mixer(docNum,trueCategory).mixWords(filteredTerms);//get terms and frequencies: ArrayList<Term>
        }
        catch(Exception e){}
        return null;
    }
    public ArrayList<Term> getTermVectorsByDocumentWithFilePath(Document d)
    {
        try
        {
            String html=new ReadIn().readStr(d.getFilePath());
            String docNum=d.getTitle();
            String trueCategory=d.getTrueCategory();
            String content=FetchContent.fetch(html);//get Chinese words
            String wordsStr=DivideChinese.divideByICTCLAS(content);//divide into words
            String[] allTerms=this.getAllTerms(wordsStr);//get all terms
            ArrayList<String> noun=this.getTermsBySplitter(allTerms, "/n");//get noun
            ArrayList<String> verb=this.getTermsBySplitter(allTerms, "/v");//get verb
            ArrayList<String> temp=this.mixArrayList(noun, verb);//mix noun+verb
            ArrayList<String> filteredTerms=this.filterByStopWords(temp);//filter out stop words
            if(globalTermsVectors!=null&&WebClassifier.getInstance().termFilterBtn.isSelected())
                filteredTerms=this.filterByGlobalTerms(filteredTerms, globalTermsVectors);
            return new Mixer(docNum,trueCategory).mixWords(filteredTerms);//get terms and frequencies: ArrayList<Term>
        }
        catch(Exception e){}
        return null;
    }

    public ArrayList<Term> getTermVectorsByDocument(String html,String docNum,String trueCategory)
    {
        try
        {
            String content=FetchContent.fetch(html);//get Chinese words
            String wordsStr=DivideChinese.divideByICTCLAS(content);//divide into words
            String[] allTerms=this.getAllTerms(wordsStr);//get all terms
            ArrayList<String> noun=this.getTermsBySplitter(allTerms, "/n");//get noun
            ArrayList<String> verb=this.getTermsBySplitter(allTerms, "/v");//get verb
            ArrayList<String> temp=this.mixArrayList(noun, verb);//mix noun+verb
            ArrayList<String> filteredTerms=this.filterByStopWords(temp);//filter out stop words
            if(globalTermsVectors!=null&&WebClassifier.getInstance().termFilterBtn.isSelected())
                filteredTerms=this.filterByGlobalTerms(filteredTerms, globalTermsVectors);
            return new Mixer(docNum,trueCategory).mixWords(filteredTerms);//get terms and frequencies: ArrayList<Term>
        }
        catch(Exception e)
        {
        }
        return null;
    }
    public ArrayList<Term> getTitleVectors(String title,String trueCategory)
    {
        try
        {
            String content=FetchContent.fetch(title);//get Chinese words
            String wordsStr=DivideChinese.divideByICTCLAS(content);//divide into words
            String[] allTerms=this.getAllTerms(wordsStr);//get all terms
            ArrayList<String> noun=this.getTermsBySplitter(allTerms, "/n");//get noun
            ArrayList<String> verb=this.getTermsBySplitter(allTerms, "/v");//get verb
            ArrayList<String> temp=this.mixArrayList(noun, verb);//mix noun+verb
            ArrayList<String> filteredTerms=this.filterByStopWords(temp);//filter out stop words
            return new Mixer(title,trueCategory).mixWords(filteredTerms);//get terms and frequencies: ArrayList<Term>
        }
        catch(Exception e)
        {
        }
        return null;
    }



    public ArrayList<Term> getGlobalTermVectors()
    {
        this.documentList=new ReadIn().getDocumentListByDirectory("./pages/");
        return new Mixer().mixTermVectors(documentList);
    }
    public ArrayList<Term> getGlobalTermVectors(ArrayList<Document> documentList)
    {
        this.documentList=documentList;
        return new Mixer().mixTermVectors(documentList);
    }

    public ArrayList<Term> getGlobalTermVectorsWithCHI()
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectorsWithIG();
        globalTermVectors=calculateGlobalTermVectorsWithCHI(globalTermVectors);
        return globalTermVectors;
    }    
    public ArrayList<Term> getGlobalTermVectorsWithCHI(ArrayList<Document> documentList)
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectors(documentList);
        globalTermVectors=calculateGlobalTermVectorsWithCHI(globalTermVectors);
        return globalTermVectors;
    }

    private ArrayList<Term> calculateGlobalTermVectorsWithCHI(ArrayList<Term> globalTermVectors)
    {
        long n=0;//total number of all terms
        for(Term t:globalTermVectors)
        {
            for(Record r:t.getRecordsList())
            {
                n+=r.getFrequency();
            }
        }
        int i=0;
        int total=globalTermVectors.size();
        for(Term t:globalTermVectors)
        {
            i++;
            long[] n11n12Info=calculaten11n12(t,"information");
            long[] n11n12Down=calculaten11n12(t,"download");
            long[] n11n12Yellow=calculaten11n12(t,"yellow");
            long[] n21n22Info=new long[2];
            long[] n21n22Down=new long[2];
            long[] n21n22Yellow=new long[2];
            for(Term tt:globalTermVectors)
            {
                if(!tt.getName().equals(t.getName()))
                {
                    n21n22Info[0]+=calculaten11n12(tt,"information")[0];
                    n21n22Info[1]+=calculaten11n12(tt,"information")[1];
                    n21n22Down[0]+=calculaten11n12(tt,"download")[0];
                    n21n22Down[1]+=calculaten11n12(tt,"download")[1];
                    n21n22Yellow[0]+=calculaten11n12(tt,"yellow")[0];
                    n21n22Yellow[1]+=calculaten11n12(tt,"yellow")[1];
                }
            }
            double chiInfo=calculateCHI(n11n12Info[0],n11n12Info[1],n21n22Info[0],n21n22Info[1],n);
            double chiDown=calculateCHI(n11n12Down[0],n11n12Down[1],n21n22Down[0],n21n22Down[1],n);
            double chiYellow=calculateCHI(n11n12Yellow[0],n11n12Yellow[1],n21n22Yellow[0],n21n22Yellow[1],n);
            /*
            double temp=Arith.max(chiInfo, chiDown, chiYellow);
            chiInfo=Arith.div(chiInfo, temp);
            chiDown=Arith.div(chiDown, temp);
            chiYellow=Arith.div(chiYellow, temp);
             * 
             */
            t.setCHIInfo(chiInfo);
            t.setCHIDown(chiDown);
            t.setCHIYellow(chiYellow);
            System.out.println(i+"/"+total+" CHI Completed!");
        }
        return globalTermVectors;
    }

    private long[] calculaten11n12(Term t, String category)
    {
        long[] n11n12=new long[2];
        long n11=0;
        long n12=0;
        ArrayList<Record> records=t.getRecordsList();
        for(Record r:records)
        {
            if(r.getTrueCategory().equals(category))
                n11+=r.getFrequency();
            else n12+=r.getFrequency();
        }
        n11n12[0]=n11;
        n11n12[1]=n12;
        return n11n12;
    }
    private double calculateCHI(long n11,long n12,long n21,long n22,long n)
    {
        double chi=0.0;
        long part1=n11*n22-n12*n21;
        long part2=n11+n12;
        long part3=n21+n22;
        long part4=n11+n21;
        long part5=n12+n22;
        BigDecimal bigN=new BigDecimal(new String(String.valueOf(n)));
        BigDecimal bigPart1=new BigDecimal(new String(String.valueOf(part1)));
        BigDecimal bigPart2=new BigDecimal(new String(String.valueOf(part2)));
        BigDecimal bigPart3=new BigDecimal(new String(String.valueOf(part3)));
        BigDecimal bigPart4=new BigDecimal(new String(String.valueOf(part4)));
        BigDecimal bigPart5=new BigDecimal(new String(String.valueOf(part5)));
        //long numerator=(long)n*part1*part1;
        double numerator=bigN.multiply(bigPart1).multiply(bigPart1).doubleValue();
        //long denominator=(long)part2*part3*part4*part5;
        double denominator=bigPart2.multiply(bigPart3).multiply(bigPart4).multiply(bigPart5).doubleValue();
        //chi=(double)numerator/(double)denominator;
        chi=Arith.div((double)numerator, (double)denominator);
        return chi;
    }

    public ArrayList<Term> getGlobalTermVectorsWithTfidf()
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectors();
        int N=this.getPagesCount();
        for(Term t:globalTermVectors)
        {
            int n=t.getRecordsList().size();
            for(Record r:t.getRecordsList())
            {
                int tf=r.getFrequency();
                double idf=log2((double)N/n+0.01);//algorithm of tf-idf
                r.setTFIDF(tf*idf);
            }
        }
        return globalTermVectors;
    }
    public ArrayList<Term> getGlobalTermVectorsWithTfidf(ArrayList<Document> documentList)
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectors(documentList);
        int N=this.getPagesCount();
        for(Term t:globalTermVectors)
        {
            int n=t.getRecordsList().size();
            for(Record r:t.getRecordsList())
            {
                int tf=r.getFrequency();
                double idf=log2((double)N/n+0.01);//algorithm of tf-idf
                r.setTFIDF(tf*idf);
            }
        }
        return globalTermVectors;
    }

    public ArrayList<Term> getGlobalTermVectorsWithIG()
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectorsWithTfidf();
        ArrayList<String> informationTitles=new ReadIn().readTitles("./pages/information/");
        ArrayList<String> downloadTitles=new ReadIn().readTitles("./pages/download/");
        ArrayList<String> yellowTitles=new ReadIn().readTitles("./pages/yellow/");
        
        int pagesCount=this.getPagesCount();
        double pci=1.0/3.0;//total three categories
        double logpci=log2(pci);
        double first=-pci*logpci*3.0;
        for(Term t:globalTermVectors)
        {
            int tCount=t.getRecordsList().size();
            if(tCount==pagesCount)
            {
                t.setIG(0.0);//if t appears in every page, set IG to be zero
                continue;
            }
            double second=this.calculateSecond(tCount,pagesCount,informationTitles,downloadTitles,yellowTitles,t.getRecordsList());
            double third=this.calculateThird(tCount,pagesCount,informationTitles,downloadTitles,yellowTitles,t.getRecordsList());
            double ig=first+second+third;
            t.setIG(ig);
        }
        return globalTermVectors;
    }
    public ArrayList<Term> getGlobalTermVectorsWithIG(ArrayList<Document> documentList)
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectorsWithTfidf(documentList);
        ArrayList<String> informationTitles=new ArrayList<String>();
        ArrayList<String> downloadTitles=new ArrayList<String>();
        ArrayList<String> yellowTitles=new ArrayList<String>();
        for(Document d:documentList)
        {
            if(d.getTrueCategory().equals("information"))
                informationTitles.add(d.getTitle());
            else if(d.getTrueCategory().equals("download"))
                downloadTitles.add(d.getTitle());
            else if(d.getTrueCategory().equals("yellow"))
                yellowTitles.add(d.getTitle());
        }

        int pagesCount=this.getPagesCount();
        double pci=1.0/3.0;//total three categories
        double logpci=log2(pci);
        double first=-pci*logpci*3.0;
        for(Term t:globalTermVectors)
        {
            int tCount=t.getRecordsList().size();
            if(tCount==pagesCount)
            {
                t.setIG(0.0);//if t appears in every page, set IG to be zero
                continue;
            }
            double second=this.calculateSecond(tCount,pagesCount,informationTitles,downloadTitles,yellowTitles,t.getRecordsList());
            double third=this.calculateThird(tCount,pagesCount,informationTitles,downloadTitles,yellowTitles,t.getRecordsList());
            double ig=first+second+third;
            t.setIG(ig);
        }
        return globalTermVectors;
    }

    public ArrayList<Term> getFullGlobalTermVectors(ArrayList<Document> documentList)
    {
        ArrayList<Term> globalTermVectors=this.getGlobalTermVectorsWithIG(documentList);
        globalTermVectors=this.calculateGlobalTermVectorsWithCHI(globalTermVectors);
        return globalTermVectors;
    }

    private double calculateSecond(int tCount,int pagesCount,ArrayList<String> informationTitles,ArrayList<String> downloadTitles,ArrayList<String> yellowTitles,ArrayList<Record> records)
    {
        double second=0.0;
        double pt=(double)tCount/pagesCount;
        double pcit1=(double)this.getCountOfInfoPagesOnT(informationTitles,records)/tCount;//for information pages
        double pcit2=(double)this.getCountOfDownPagesOnT(downloadTitles,records)/tCount;//for download pages
        double pcit3=(double)this.getCountOfYellPagesOnT(yellowTitles,records)/tCount;//for yellow pages
        double logpcit1=log2(pcit1);
        double logpcit2=log2(pcit2);
        double logpcit3=log2(pcit3);
        second=pt*(pcit1*logpcit1+pcit2*logpcit2+pcit3*logpcit3);
        return second;
    }

    private double calculateThird(int tCount,int pagesCount,ArrayList<String> informationTitles,ArrayList<String> downloadTitles,ArrayList<String> yellowTitles,ArrayList<Record> records)
    {
        double third=0.0;
        int nCount=pagesCount-tCount;
        double pn=(double)nCount/pagesCount;
        double pcin1=(double)this.getCountOfInfoPagesOffT(informationTitles,records)/nCount;
        double pcin2=(double)this.getCountOfDownPagesOffT(downloadTitles,records)/nCount;
        double pcin3=(double)this.getCountOfYellPagesOffT(yellowTitles,records)/nCount;
        double logpcin1=log2(pcin1);
        double logpcin2=log2(pcin2);
        double logpcin3=log2(pcin3);
        third=pn*(pcin1*logpcin1+pcin2*logpcin2+pcin3*logpcin3);
        return third;
    }

    private int getCountOfInfoPagesOnT(ArrayList<String> informationTitles,ArrayList<Record> records)
    {
        //calculate number of pages that contain t whose trueCategory is information
        int count=0;
        for(Record r:records)
        {
            if(informationTitles.contains(r.getDocNum()))
                count+=1;
        }
        return count;
    }
    private int getCountOfDownPagesOnT(ArrayList<String> downloadTitles,ArrayList<Record> records)
    {
        //calculate number of pages that contain t whose trueCategory is download
        int count=0;
        for(Record r:records)
        {
            if(downloadTitles.contains(r.getDocNum()))
                count+=1;
        }
        return count;
    }
    private int getCountOfYellPagesOnT(ArrayList<String> yellowTitles,ArrayList<Record> records)
    {
        //calculate number of pages that contain t whose trueCategory is yellow
        int count=0;
        for(Record r:records)
        {
            if(yellowTitles.contains(r.getDocNum()))
                count+=1;
        }
        return count;
    }

    private int getCountOfInfoPagesOffT(ArrayList<String> informationTitles,ArrayList<Record> records)
    {
        //calculate number of pages that do not contain t whose trueCategory is information
        int count=0;
        int infoCount=informationTitles.size();
        int infoOnT=0;
        for(Record r:records)
        {
            if(informationTitles.contains(r.getDocNum()))
                infoOnT+=1;
        }
        count=infoCount-infoOnT;
        return count;
    }
    private int getCountOfDownPagesOffT(ArrayList<String> downloadTitles,ArrayList<Record> records)
    {
        //calculate number of pages that do not contain t whose trueCategory is download
        int count=0;
        int downCount=downloadTitles.size();
        int downOnT=0;
        for(Record r:records)
        {
            if(downloadTitles.contains(r.getDocNum()))
                downOnT+=1;
        }
        count=downCount-downOnT;
        return count;
    }
    private int getCountOfYellPagesOffT(ArrayList<String> yellowTitles,ArrayList<Record> records)
    {
        //calculate number of pages that do not contain t whose trueCategory is yellow
        int count=0;
        int yellowCount=yellowTitles.size();
        int yellowOnT=0;
        for(Record r:records)
        {
            if(yellowTitles.contains(r.getDocNum()))
                yellowOnT+=1;
        }
        count=yellowCount-yellowCount;
        return count;
    }

    public int getPagesCount()
    {
        int count=0;
        File dir=new File("./pages/");
        File[] f=dir.listFiles();
        for(File tf:f)
        {
            if(tf.isDirectory())
            {
                File[] ff=new File(tf.getPath()+"/").listFiles();
                count+=ff.length;
            }
        }
        return count;
    }

    private double log2(double a)
    {
        if(a==0)
            return 0;
        Double b=Math.log(a)/Math.log(2);        
        return b;
    }

    private String[] getAllTerms(String wordsStr)
    {
        return wordsStr.split(" ");
    }

    private ArrayList<String> getTermsBySplitter(String[] allTerms, String splitter)
    {
        ArrayList<String> terms=new ArrayList<String>();
        for(int i=0;i<allTerms.length;i++)
        {
            if(allTerms[i].contains(splitter))
                terms.add(allTerms[i].split(splitter)[0]);
        }
        return terms;
    }

    private ArrayList<String> filterByStopWords(ArrayList<String> oldTerms)
    {
        ArrayList<String> stopWords=new ReadIn().readStopwords("stopwords.txt");
        return new Mixer().filterStopWords(oldTerms, stopWords);
    }

    private ArrayList<String> mixArrayList(ArrayList<String> list1,ArrayList<String> list2)
    {
        ArrayList<String> newList=new ArrayList<String>();
        for(String s:list1)
        {
            newList.add(s);
        }
        for(String s:list2)
        {
            newList.add(s);
        }
        return newList;
    }

    private ArrayList<String> filterByGlobalTerms(ArrayList<String> filteredTerms, ArrayList<Term> globalTermVectors)
    {
        ArrayList<String> globalTerms=new ArrayList<String>();
        for(Term t:globalTermVectors)
        {
            globalTerms.add(t.getName());
        }
        return new Mixer().filterNonStopWords(filteredTerms, globalTerms);
    }


}
