/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.util.ArrayList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Administrator
 */
public class Prefilter {

    private double aToPRatioThreshold;
    private double aToInnerRatioThreshold;
    public Prefilter()
    {
        Config config=new Config("Parameters.props");
        aToPRatioThreshold=Double.parseDouble(config.getValue("aToPRatioThreshold"));
        aToInnerRatioThreshold=Double.parseDouble(config.getValue("aToInnerRatioThreshold"));

    }
    public Boolean detectHub(MainFrame.Document d)
    {
        //Boolean isHub=this.getAtoPRatio(d);
        Boolean isHub=this.detectByDocument(d);
        //detect whether this page is a hub page
        return isHub;
    }

    private Boolean detectByDocument(MainFrame.Document d)
    {
        Boolean isHub=false;     
        try
        {
            Document newD;
            String html=d.getHtml();
            String filePath=d.getFilePath();
            if(html!=null)
                newD=Jsoup.parse(html);
            else
            {
                File f=new File(filePath);
                newD=Jsoup.parse(f, "GBK");
            }
            String innerText=newD.text();
            Elements anchors=newD.getElementsByTag("a");
            Elements paragraphs=newD.getElementsByTag("p");
            int aCount=anchors.size();
            int pCount=paragraphs.size();
            String aText=null;
            for(Element a:anchors)
            {
                aText+=a.text();
            }
            String pText=null;
            for(Element p:paragraphs)
            {
                pText+=p.text();
            }
            int aTextCount=1;
            int pTextCount=1;
            int innerTextCount=1;
            if(aText!=null &&pText!=null&&innerText!=null)
            {
                aTextCount=aText.length();
                pTextCount=pText.length();
                innerTextCount=innerText.length();
            }

            int aThres=200;
            int aThres1=500;
            int pThres=20;
            double ratio1Thres=3;
            double ratio2Thres=0.65;
            double ratio1=(double)aTextCount/(double)pTextCount;
            double ratio2=(double)aTextCount/(double)innerTextCount;
            System.out.println("Anchors To Paragraphs Ratio: "+ratio1);
            System.out.println("Anchors To Whole Ratio: "+ratio2);
            if(ratio1>aToPRatioThreshold && ratio2>aToInnerRatioThreshold)
            {
                isHub=true;
            }
            else isHub=false;
            return isHub;
        }
        catch(Exception e){System.out.println("Document Information Incompleted!");}
        return null;
    }


    private Boolean getAtoPRatio(MainFrame.Document d)
    {
        Boolean isHub=false;
        String html=d.getHtml();
        Document newD=Jsoup.parse(html);
        d.setTitle(newD.title());
        ArrayList<Term> terms=new GetTermVectors().getTermVectorsByDocument(d);
        Elements anchors=newD.getElementsByTag("a");
        Elements paragraphs=newD.getElementsByTag("p");
        String innerText=newD.text();
        int downloadCount=0;//得到“下载”字样个数
        int yellowCount=0;
        int softwareCount=0;
        int companyCount=0;
        for(Term t:terms)
        {
            if(t.getName().equals("下载"))
                downloadCount=t.getRecordsList().get(0).getFrequency();
            if(t.getName().equals("黄页"))
                yellowCount=t.getRecordsList().get(0).getFrequency();
            if(t.getName().equals("软件"))
                softwareCount=t.getRecordsList().get(0).getFrequency();
            if(t.getName().equals("有限公司"))
                companyCount=t.getRecordsList().get(0).getFrequency();
        }
        int aCount=anchors.size();
        int pCount=paragraphs.size(); 
        String aText=null;
        for(Element a:anchors)
        {
            aText+=a.text();
        }
        String pText=null;
        for(Element p:paragraphs)
        {
            pText+=p.text();
        }
        int aTextCount=aText.length();
        int pTextCount=pText.length();
        int innerTextCount=innerText.length();
        int aThres=200;
        int aThres1=500;
        int pThres=20;
        double ratio1Thres=3;
        double ratio2Thres=0.65;
        double ratio1=(double)aTextCount/(double)pTextCount;
        double ratio2=(double)aTextCount/(double)innerTextCount;
  
        if(ratio1>ratio1Thres && ratio2>ratio2Thres)
        {
            isHub=true;
        }
        else isHub=false;
    
        System.out.println("aCount:"+aCount);
        System.out.println("pCount:"+pCount);
        System.out.println("aTextCount:"+aTextCount);
        System.out.println("pTextCount:"+pTextCount);
        System.out.println("innerTextCount:"+innerTextCount);
        System.out.println("黄页Count:"+yellowCount);
        System.out.println("下载Count:"+downloadCount);
        System.out.println("软件Count:"+softwareCount);
        System.out.println("有限公司Count:"+companyCount);

        return isHub;
    }
}
