/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

/**
 *
 * @author Administrator
 */
public class RealtimeClassifyThread extends AbstractThread{

    public RealtimeClassifyThread(WebClassifier wc, String fileName)
    {
        super(wc,fileName);
    }

    public void run()
    {
        try
        {
            init();
            initDirectory();
            for(File f:fs)
            {
                if(f.isDirectory())
                {
                    File page=new File(f.getPath()+"/");
                    File[] pages=page.listFiles();
                    String trueCategory=f.getName();        
                    ArrayList<Term> vectors;
                    ArrayList<Term> titleVectors;
                    GetTermVectors gtv=new GetTermVectors(wc.globalTermVectors);
                    Document d=new Document();
                    String title;
                    String category;
                    Prefilter pf=new Prefilter();
                    for(File p:pages)
                    {
                        if(stop)
                            break;
                        value++;
                        
                        category="";
                        title=ri.getTitles(p.getName());
                        if(title!=null)
                        {
                            vectors=gtv.getTermVectorsByDocument(ri.readStr(p.getPath()),title,trueCategory);
                            titleVectors=gtv.getTitleVectors(title, trueCategory);
                            d.setTitle(title);
                            d.setTitleVector(titleVectors);
                            d.setFilePath(p.getPath());
                            d.setTrueCategory(trueCategory);
                            d.setTermVector(vectors);
                            System.out.println(title+" Ready!"+"  "+value+"/"+total);
                            if(wc.hubFilterBtn.isSelected())
                            {
                                if(pf.detectHub(d))
                                    category="others";
                            }
                            if(!category.equals("others"))
                            {
                                GetCategory getC=new GetCategory(wc.globalTermVectors,wc.documentList);
                                d=getC.getClassifiedDocumentByDocument(d);
                                category=d.getCategory();
                            }
                            System.out.println(title+" Completed!"+" "+value+"/"+total+" "+category);
                            wc.resultUrlText.setText(title+" Completed!");
                            Object[] arr=new Object[8];
                            arr[0]=title;
                            arr[1]=trueCategory;
                            arr[2]=category;
                            arr[3]=d.getSimilarity();
                            arr[4]=d.getScore();
                            arr[5]=d.sInfo;
                            arr[6]=d.sDown;
                            arr[7]=d.sYellow;
                            tm.addRow(arr);
                            wc.resultNumberText.setText(String.valueOf(value));
                            SwingUtilities.invokeLater(target);
                        }

                    }
                }
            }
            
        }
        catch(Exception ex){System.out.println("Realtime Classification Errors!");ex.printStackTrace();}
        finally
        {
            noticeCompleted("Realtime Classification Completed!");
        }
    }

}
