/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Mixer {
    private String title;
    private String trueCategory;
    private int lowFrequencyThreshold;

    public Mixer()
    {

    }
    public Mixer(String title, String trueCategory)
    {
        title=FetchContent.fetch(title);
        this.title=title;
        this.trueCategory=trueCategory;
        Config config=new Config("Parameters.props");
        lowFrequencyThreshold=Integer.parseInt(config.getValue("lowFrequencyThreshold"));
    }


    public ArrayList<Term> mixWords(ArrayList<String> oldWords)
    {
        //mix terms that have same name and count the frequency of a page
        ArrayList<Term> newWords=new ArrayList<Term>();
        ArrayList<String> temp=new ArrayList<String>();
        for(int i=0;i<oldWords.size();i++)
        {
            if(!temp.contains(oldWords.get(i)))
            {
                temp.add(oldWords.get(i));
                Term newWord=new Term(oldWords.get(i));
                Record newRecord=new Record(title,trueCategory,1);
                newWord.getRecordsList().add(newRecord);
                newWords.add(newWord);
            }
            else
            {
                newWords.get(temp.indexOf(oldWords.get(i))).getRecordsList().get(0).addFrequency();
            }
        }
        newWords=termFrequencyHighPassFilter(newWords);
        return newWords;
    }
    private ArrayList<Term> termFrequencyHighPassFilter(ArrayList<Term> newWords)
    {
        for(int i=0;i<newWords.size();i++)
        {
            if(newWords.get(i).getRecordsList().get(0).getFrequency()<=lowFrequencyThreshold)
            {
                newWords.remove(i);
                i--;
            }
        }
        return newWords;
    }

    public ArrayList<String> filterStopWords(ArrayList<String> oldWords, ArrayList<String> stopWords)
    {
        ArrayList<String> newWords=new ArrayList<String>();
        for(int i=0;i<oldWords.size();i++)
        {
            if(!stopWords.contains(oldWords.get(i)))
                newWords.add(oldWords.get(i));
        }
        return newWords;
    }

    public ArrayList<String> filterNonStopWords(ArrayList<String> oldWords, ArrayList<String> nonStopWords)
    {
        ArrayList<String> newWords=new ArrayList<String>();
        for(int i=0;i<oldWords.size();i++)
        {
            if(nonStopWords.contains(oldWords.get(i)))
                newWords.add(oldWords.get(i));
        }
        return newWords;
    }

    public ArrayList<Term> mixTermVectors(ArrayList<Document> documentList)
    {
        //mix termvector of each page into one global template
        try
        {
            ArrayList<Term> globalTermVectors=new ArrayList<Term>();
            ArrayList<String> globalTermName=new ArrayList<String>();
            int total=documentList.size();
            int n=0;
            for(Document d:documentList)
            {
                n++;
                ArrayList<Term> ts=d.getTermVector();
                for(Term t:ts)
                {
                    if(!globalTermName.contains(t.getName()))
                    {
                        globalTermVectors.add(t);
                        globalTermName.add(t.getName());
                    }
                    else
                    {
                        for(int i=0;i<globalTermVectors.size();i++)
                        {
                            if(globalTermVectors.get(i).getName().equals(t.getName()))
                            {
                                //mix the records
                                ArrayList<String> recordDocNumList=new ArrayList<String>();
                                for(Record r:globalTermVectors.get(i).getRecordsList())
                                {
                                    recordDocNumList.add(r.getDocNum());
                                }
                                if(!recordDocNumList.contains(t.getRecordsList().get(0).getDocNum()))
                                {
                                    globalTermVectors.get(i).getRecordsList().add(t.getRecordsList().get(0));
                                    recordDocNumList.add(t.getRecordsList().get(0).getDocNum());
                                }
                                else
                                {
                                    for(Record rr:globalTermVectors.get(i).getRecordsList())
                                    {
                                        if(rr.getDocNum().equals(t.getRecordsList().get(0).getDocNum()))
                                        {
                                            rr.addFrequency(t.getRecordsList().get(0).getFrequency());
                                        }
                                        break;
                                    }
                                }
                                break;
                            }
                           
                        }
                    }
                }
                System.out.println(d.getTitle()+" Mixed!"+"  "+n+"/"+total);
            }
            return globalTermVectors;
        }
        catch(Exception ex)
        {
            System.out.println("Initial GlobalTermVectors Errors!");
        }
        return null;
    }


}
