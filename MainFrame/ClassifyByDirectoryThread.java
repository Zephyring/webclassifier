/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.util.ArrayList;
import javax.swing.SwingUtilities;

/**
 *
 * @author Administrator
 */
public class ClassifyByDirectoryThread extends AbstractThread {

    public ClassifyByDirectoryThread(WebClassifier wc,String fileName)
    {
        super(wc,fileName);
    }

    public void run()
    {
        try
        {
            //resultDocuments=new GetCategory(globalTermVectors,documentList,singleSimilarityThreshold).getClassifiedDocumentListByDirectory(fileName);
            //updateResultTable();
            //JOptionPane.showMessageDialog(this, "Classification Completed!");
            //mainFrame.setSelectedIndex(4);
            init();
            initDirectory();
            ArrayList<Document> resultDocuments=new ArrayList<Document>();
            for(File f:fs)
            {
                if(f.isDirectory())
                {
                    File page=new File(f.getPath()+"/");
                    File[] pages=page.listFiles();
                    String trueCategory=f.getName();
                    String title;
                    ArrayList<Term> vectors;
                    ArrayList<Term> titleVectors;
                    Document d;
                    GetTermVectors gtv=new GetTermVectors(wc.globalTermVectors);
                    for(File p:pages)
                    {
                        if(stop)
                            break;
                        value++;
                        title=ri.getTitles(p.getName());
                        if(title!=null)
                        {
                            d=new Document();
                            vectors=gtv.getTermVectorsByDocument(ri.readStr(p.getPath()),title,trueCategory);
                            titleVectors=gtv.getTitleVectors(title, trueCategory);
                            d.setTitle(title);
                            d.setFilePath(p.getPath());
                            d.setTrueCategory(trueCategory);
                            d.setTermVector(vectors);
                            d.setTitleVector(titleVectors);
                            resultDocuments.add(d);                     
                            Object[] arr=new Object[4];
                            arr[0]=title;
                            arr[1]=trueCategory;
                            tm.addRow(arr);
                            wc.resultNumberText.setText(String.valueOf(value));
                            SwingUtilities.invokeLater(target);
                            System.out.println(title+" Ready!"+"  "+value+"/"+total);
                            wc.resultUrlText.setText(title+" Ready!");
                        }

                    }
                }
            }
            value=0;
            GetCategory getC=new GetCategory(wc.globalTermVectors,wc.documentList);
            String category;
            double similarity;
            Prefilter pf=new Prefilter();
            for(Document d:resultDocuments)
            {
                if(stop)
                    break;
                category="";
                similarity=0.0;
                if(wc.hubFilterBtn.isSelected())
                {
                    if(pf.detectHub(d))
                    {
                        category="others";
                        similarity=0.0;
                    }
                }
                if(!category.equals("others"))
                {
                    d=getC.getClassifiedDocumentByDocument(d);
                    category=d.getCategory();
                    similarity=d.getSimilarity();
                }
                String title=d.getTitle();
                tm.setValueAt(category, value, 2);
                tm.setValueAt(similarity, value, 3);
                value++;
                wc.resultNumberText.setText(String.valueOf(value));
                SwingUtilities.invokeLater(target);
                System.out.println(title+" Completed!"+" "+value+"/"+total+" "+category);
                wc.resultUrlText.setText(title+" Completed!");
            }       
        }
        catch(Exception ex){System.out.println("Classification By Directory Errors!");}
        finally
        {
            noticeCompleted("Classification By Directory Completed!");
        }
    }
}
