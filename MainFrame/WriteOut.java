/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;
import java.util.ArrayList;
import java.io.BufferedWriter;
import java.io.File;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
/**
 *
 * @author Administrator
 */
public class WriteOut {
    

    public void writeStr(String str, String fileName)
    {
        PrintWriter pw=null;
        try
        {
            pw=new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fileName),"gbk")));
            pw.print(str);
            pw.flush();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
        finally
        {
            if(pw!=null)
            {
                pw.close();
            }
        }
    }

    public void writeStrAtEnd(String str, String fileName)
    {
        try
        {
            RandomAccessFile rf=new RandomAccessFile(fileName,"rw");
            Long fileLength=rf.length();
            rf.seek(fileLength);
            rf.writeBytes(str);
            rf.close();
        }
        catch(Exception ex)
        {
        }
    }

    public void writeHtmls(String urlListFileName,String category)
    {
        ArrayList<String> urlList=new ReadIn().readUrlList(urlListFileName);
        File f=new File("./pages/"+category);
        File urlF=new File("URLs.props");
        try
        {
            if(!f.exists())
                f.mkdir();
            if(!urlF.exists())
                urlF.createNewFile();
            Config config=new Config("URLs.props");
            System.out.println("Total Size: "+urlList.size());
            for(int i=0;i<urlList.size();i++)
            {
                String urlStr=urlList.get(i);
                org.jsoup.nodes.Document d=new GetHtml().getJsoupDocumentByUrl(urlStr);
                if(d!=null)
                {
                    String html=d.outerHtml();
                    String title=FetchContent.fetch(d.title());
                    if(title!=null)
                    {
                        this.writeStr(html, "./pages/"+category+"/"+title+".txt");
                        config.setValue(title, urlStr);
                        System.out.println(title+"  "+(i+1)+"/"+urlList.size());
                    }
                }
            }
            config.saveFile();
            System.out.println("Finished");
           
        }
        catch(Exception ex)
        {
            System.out.println("Errors!");
        }
    }

    

    public void saveGlobalTermVectors(ArrayList<Term> globalTermVectors)
    {
        ConnectSql conn=new ConnectSql();
        conn.insert("delete from globalTermVectors");
        conn.insert("delete from globalTermVectorsRecords");
        for(Term t:globalTermVectors)
        {
            String name=t.getName();
            String ig=String.valueOf(t.getIG());
            String chiInfo=String.valueOf(t.getCHIInfo());
            String chiDown=String.valueOf(t.getCHIDown());
            String chiYellow=String.valueOf(t.getCHIYellow());
            conn.insert("insert into globalTermVectors (name, ig, chiInfo, chiDown, chiYellow) values('"+name+"','"+ig+"','"+chiInfo+"','"+chiDown+"','"+chiYellow+"');");
            for(Record r:t.getRecordsList())
            {
                String docNum=r.getDocNum();
                String frequency=String.valueOf(r.getFrequency());
                String tfidf=String.valueOf(r.getTFIDF());
                conn.insert("insert into globalTermVectorsRecords (docNum, frequency, tfidf, name) values('"+docNum+"','"+frequency+"','"+tfidf+"','"+name+"');");
            }
        }
    }

    public void saveDocumentList(ArrayList<Document> documentList)
    {
        ConnectSql conn=new ConnectSql();
        conn.insert("delete from documents");
        conn.insert("delete from titleVectors");
        for(Document d:documentList)
        {
            String category=d.getTrueCategory();
            String title=d.getTitle();
            conn.insert("insert into documents (category, title) values('"+category+"','"+title+"');");
            for(Term t:d.getTitleVector())
            {
                String docNum=t.getRecordsList().get(0).getDocNum();
                String frequency=String.valueOf(t.getRecordsList().get(0).getFrequency());
                String name=t.getName();
                conn.insert("insert into titleVectors (docNum, frequency, name) values('"+docNum+"','"+frequency+"','"+name+"');");
            }
        }
    }

}
