/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;


/**
 *
 * @author Administrator
 */
public class Config {

    private Properties p;
    private FileInputStream in;
    private String fileName;
    public Config(String fileName)
    {
        try
        {
            this.fileName=fileName;
            File f=new File(fileName);
            if(!f.exists())
                f.createNewFile();
            in=new FileInputStream(fileName);
            p=new Properties();
            p.load(in);
            in.close();
        }
        catch(Exception ex)
        {
            System.out.println("Initial configuration fails!");
        }
    }

    public String getValue(String key)
    {
        return p.getProperty(key);
    }

    public void setValue(String key, String value)
    {
        p.setProperty(key, value);
    }

    public void saveFile()
    {
        try
        {
            FileOutputStream out=new FileOutputStream(fileName);
            p.store(out, null);
            out.close();
        }
        catch(Exception ex)
        {
            System.out.println("Save Configuration File fails!");
        }
    }

    public Properties getProperties()
    {
        return this.p;
    }


}
