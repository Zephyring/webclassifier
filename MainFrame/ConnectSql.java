
package MainFrame;
import java.sql.*;

/**
 *
 * @author Administrator
 */
public class ConnectSql {
    //String DBDriver="sun.jdbc.odbc.JdbcOdbcDriver";
    String DBDriver;
    //String ConnStr="jdbc:odbc:Classifier";
    String ConnStr;
    ResultSet rs=null;
    String user;
    String password;
    Connection conn=null;
    String configFile="Database.props";
    
    public ConnectSql()
    {
        try
        {
            Config config=new Config(configFile);
            DBDriver=config.getValue("DBDriver");
            ConnStr=config.getValue("ConnStr");
            user=config.getValue("user");
            password=config.getValue("password");
            config.saveFile();
            Class.forName(DBDriver).newInstance();
            conn=DriverManager.getConnection(ConnStr,user,password);
        }
        catch(Exception ex)
        {
        }
    }

    public void insert(String sql)
    {
        try
        {
            Statement stm=conn.createStatement();
            stm.execute(sql);
        }
        catch(Exception e)
        {
            
        }
    }

    public ResultSet query(String sql)
    {
        try
        {
            Statement stm=conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs=stm.executeQuery(sql);
            return rs;
        }
        catch(Exception e)
        {
            
        }
        return null;
    }

    public Connection getConnection()
    {
        return this.conn;
    }
    

}
