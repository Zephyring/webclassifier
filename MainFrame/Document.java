/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package MainFrame;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Document {
    private String html;
    private String title;
    private String url;
    private ArrayList<Term> termVector;
    private ArrayList<Term> titleVector;
    private String category;
    private String trueCategory;
    private String filePath;
    private double similarity;
    private double score;
    public double sInfo;
    public double sDown;
    public double sYellow;

    public String getHtml(){return this.html;}
    public void setHtml(String html){this.html=html;}
    public String getTitle(){return this.title;}
    public void setTitle(String title){this.title=title;}
    public String getUrl(){return this.url;}
    public void setUrl(String url){this.url=url;}
    public ArrayList<Term> getTermVector(){return this.termVector;}
    public void setTermVector(ArrayList<Term> termVector){this.termVector=termVector;}
    public String getCategory(){return this.category;}
    public void setTitleVector(ArrayList<Term> titleVector){this.titleVector=titleVector;}
    public ArrayList<Term> getTitleVector(){return this.titleVector;}
    public void setCategory(String category){this.category=category;}
    public String getTrueCategory(){return this.trueCategory;}
    public void setTrueCategory(String trueCategory){this.trueCategory=trueCategory;}
    public String getFilePath(){return this.filePath;}
    public void setFilePath(String filePath){this.filePath=filePath;}
    public double getSimilarity(){return this.similarity;}
    public void setSimilarity(double similarity){this.similarity=similarity;}
    public double getScore(){return score;}
    public void setScore(double score){this.score=score;}

    public Document(String html, String title, String trueCategory, ArrayList<Term> termVector)
    {
        this.html=html;
        this.title=title;
        this.trueCategory=trueCategory;
        this.termVector=termVector;
    }
    public Document()
    {
        
    }

}
